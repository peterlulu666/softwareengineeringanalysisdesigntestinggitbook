## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1Q_Cn34SfnVcDyQAm_r0J-AOl_BHITTsu/preview"></iframe>

## Quiz        

1. Domain modeling process
    - Collecting information about the application domai 
    - Brainstorming. 
    - Classifying brainstorming results. 
    - Visualizing the domain model using a UML class diagram. 
    - Performing inspection and review.

2. The process
    - The planning Phase.
    - The iterative Phase.

3. The model
    - The conceptual representation of something.
    - A schematic description of a system, theory, or phenomenon that accounts for its known or inferred properties and may be used for further study of its characteristics.

4. Why model
    - We **perceive** the world differently due to differences in backgrounds and viewpoints. Modeling facilitate collective understand of the application.   
    - Because the team members and users need to **communicate** their perceptions about a piece of reality.
    - A model facilitates team members and users **communication** of their perception and design idea.        
    - Because we need models during the **maintenance** phase to perform enhancement maintenance.      

5. domain modeling
    - What
        - A process that helps the team understand the application or application domain.
        - DM enables the team to establish a common understanding.
        -The UML.
    - Why
        - Software engineers need to work in different **domains** or different projects. They need domain knowledge to develop the system.
        - Software engineers come from different **backgrounds**, which affect their perception of the application domain.
    - How
        - Collect domain information, perform **brainstorming** and classification, and visualize the domain knowledge using a UML class diagram.        

6. The domain model defines application domain concepts in terms of classes, attributes, and relationships.        

7. The construction of the domain model
    - The benefit
        - Helps the development team or the analyst understand the application and the application domain. 
        - Lets the team members communicate effectively their understanding of the application and the application domain. 
        - Improves the communication between the development team and the customer/user in some cases. 
        - Provides a basis for the design, implementation, and maintenance.        

8. Domain model is represented by UML class diagrams.

9. The OO paradigm views the real world as consisting of
    - The objects
        - that **relate** to each other. 
        - **interact** with each other.

10. The basic build blocks and starting point are Objects.

11. The object concept
    - Class --- a class is a **type** 
        - an abstraction of objects with similar properties and behavior. 
        - an intentional definition of a collection of objects. 
    - Attribute --- defines **properties** of class of objects. 
    - Operation--- defines **behaviors** of class of objects. 
    - Object --- an **instance** of a class. 
    - Encapsulation --- defining/storing together properties and behavior of a class/object. 
    - Information hiding --- shielding implementation detail to reduce change impact to other part. of a program 
    - Polymorphism --- one thing can assume more than one form.



12. We will use UML to represent the domain model
    - The UML class diagram is a structural diagram.
        - It shows the classes, their attributes and operations, and relationships between the classes.
    - The Domain model is represented by a class diagram without showing the operations.

13. The UML class diagram
    - a class is a **type** 
        - The compact view, there is the **class name**.      
        - The expanded view, there is the **attribute** and the **function**. 
            - There is **attribute name** and **attribute type** in the attribute.      
            - There is **function name**, **parameter name**, **parameter type**, and **return type** in the function.       

14. The inheritance relation
    - **Expresses** the generalization / specialization relations between **concepts**.
    - One concept is more general/specialized than the other.
        - The extract.
        - The expanded.
    - Example: vehicle is a generalization of car, car is a specialization of vehicle.
    - It is also called “IS-A” relation.
    - The car and the boat is the vehicle.
    - The staff and the student is the user.     

15. TAhe object and the attribute
    - A noun/noun phrase can be a class or an attribute, how do we **distinguish**.
    - This is often a **challenge**.
    - Rules to apply
        - An object has an "**independent existence**" in the application/application domain.
        - The number of seats is attribute. 
        - Attributes describe **objects** or store state **information** of objects.
        - You **can enter an attribute** (value) from the keyboard, but you **cannot enter an object** from the keyboard.
        - Objects must be created by invoking a **constructor** (either explicitly or implicitly).      

16. The two test for the inheritance relationship
    - IS-A test
        - every instance of a subclass is also an instance of the superclass.
        - The car is not the engine.
    - Conformance test
        - relationships of a superclass are also relationships of subclasses. 
        - Visiting professors cannot enroll in a retirement plan at the host university.

17. The aggregation relationship
    - Expresses the fact that one object is **part of** another object.
        - The engine is **part of** a car.
    - It is the “**part-of**” relationship.        

18. The association relationship
    - Expresses a **general relationship** other than inheritance and aggregation.
        - These can be application-specific relationships between two concepts.
    - Example: "instructor teaches course" "user has account".
    - Enroll is not an inheritance or aggregation relationship.

19. The role and the association relationship

20. The multiplicity
    - One base station services zero or more mobiles and every mobile is serviced by exactly one base station.

21. The domain modeling step
    - Collecting application domain information.
        - focus on the **functional** requirements.
        - consider other **requirements** and documents.
        - consider business **descriptions**.        
    - Brainstorming.
        - list important application domain **concepts**.
        - list their **properties/attributes**.
        - list their **relationships**.
    - Classifying the domain concepts into.
        - Classes.
        - Attributes/attributevalues. 
        - Relationships.
    - Visualizing the result using a UML class diagram.
    - Review the Domain Model.

22. The brainstorming rule to apply
    1. nouns / noun phrases.
    2. "X of Y" expressions (e.g., color of car).
    3. transitive verbs.
    4. adjectives.
    5. numeric.
    6. possession expressions (has/have, possess, etc.).
    7. "constituents / part of" expressions.
    8. containment / containing expressions.
    9. "X is a Y" expressions.

23. The objects have **independent existence**, **attributes** do not.        

24. The car has **independent existence**. The **make, model, horse power, and number of seats** do not.        

25. The association class
    - An association class is used to model an association as a class.
        - Association classes often occur in many-to-one and many-to-many associations where the association itself has attributes.
    -  As an example, consider a many-to-many association between classes Person and Company.
        - The association could have properties of salary, jobClassification, startDate, and others.
        - In this case, the association is more correctly modeled as an association class with attributes rather than trying to fold the attributes into one of the classes in the association.
    -  Here are some pointers to consider when modeling with association classes.
        - You cannot attach the same class to more than one association. 
            - an association class is the association.
        - The name of the association is usually omitted since it is considered to be the same as that of the attached class.
            - Distinguish between the use of an association class as a modeling technique and the implementation of the association class.
                - There can be several ways to implement an association class.















