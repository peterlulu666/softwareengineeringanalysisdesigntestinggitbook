## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1U_tss8PaktPk8Z7sY7MLUSv8tUHxJI8s/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1ZAGzaJS6x8DvIoynMhov79Xi27Sh3uDB/preview"></iframe>


## Quiz        

1. Guidelines for Practicing Coding Standards
    - Define **barely enough** coding standards 
    - The coding **standards** should be easy to **understand** and **practice** 
    - The coding **standards** should be **documented** and include **examples**
    - **Training** on how to use the coding **standards** is helpful, recommended 
    - The coding standards, once defined and published, should be **practiced**, **enforced**, and **checked** for compliance regularly 
        - Non-compliant code should be identified and changed 
        - Procedures for allowing non-compliant code after senior review 
    - It is important to assign the responsibilities to individuals and make sure that all involved know the **assignments** 
    - The practice of coding **standards** should involve project and company **stakeholders**

2. Organizing the Implementation Artifacts
    - Software Architecture defined in the high level design phase suggests the local organization of the software work products/process artifacts 
        - **Architectural-style organization**: Classes  are organized according to an architectural style 
        - **Functional **subsystem** organization**: Classes are organized according to the functional subsystems of the software system 
        - **Hybrid organizations**: combination of 
            - Architectural-style functional **subsystem** organization 
            - Functional **subsystem** architectural-style organization

3. N-ter Architecture
    - Sequence diagram showing layers of a system
    - Corresponding N-Tier architecture layers correspond to sequence diagram layers. Layers represent components and are implemented as packages
    - <img src="img/N-ter Architecture.png" alt="N-ter Architecture" width="500" height="600">

4. Corresponding Directory Structure
    - Figure is result of applying functional subsystem organization to previous example 
    - Classes belonging to the subsystems are organized into these subdirectories 
        - Keeps process artifacts, code well organized 
        - Facilitates reuse
    - <img src="img/Corresponding Directory Structure.png" alt="Corresponding Directory Structure" width="500" height="600">

5. Generating Code: From Seg Diagram to Code
    - <img src="img/Generating Code: From Seg Diagram to Code.png" alt="Generating Code: From Seg Diagram to Code" width="500" height="600">

6. Implementing Association Relationships
    - There are several approaches to implementing **association** relationships 
        - A **one-to-one** association between class A and class B is implemented by: 
            - A holding a reference to B if A calls a function of B, and/or 
            - B holding a reference to A if B calls a function of A 
        - A **one-to-many** association between is implemented by: 
            - A holding a collection of references to B if A calls functions of B instances, or 
            - B holding a reference to A if instances of B call a function of A 
        - A **many-to-many** association is implemented by a collection of references from A to B and vice versa

7. Automatically Generating Code from Design
    - Many IDEs can now **generate** code skeletons from design artifacts 
    - Some tools/IDEs, depending on the details provided in the tool, can **generate** most of the system code 
        - Depends on type of system, tools chosen – May need to add code parts to model 
        - May need to assemble components with code after generation 
    - UML diagrams 
        - Sequence diagrams → source code skeleton 
        - Programmer may still need to add parts to the skeleton 
    - Other diagrams/tools code-**generators** 
    - Programming team needs to understand needs of the project and specific capabilities and limitations of the IDE tool to leverage code **generation** capability

8. Assigning Implementation Work
    - Assign classes to team members according to: 
        - Dependencies between classes (see DCD) 
        - Abilities of the team members 
        - . . . to reduce number of test stubs 
            - Test stub := Dummy Class 
            - Testing costs $$$ 
    - Implement classes 
        - Pair Programming 
        - Test driven development

9. Pair Programming
    - Two developers program at one machine simultaneously 
    - They discuss how to **implement** the features 
    - The one with the keyboard and mouse **implements** the functionality, the other **reviews** the program as it is being typed 
    - The developers **switch** roles periodically, whenever they like, or between programming sessions 
    - Each session lasts one to three hours 
    - Pairs are not fixed, they **switch** around all the time 
    - All team members are required to work with others
        - If two people cannot work together, they don’t have to pair with each other

10. Merits of Pair Programming
    - It **reduces pressure** and brings fun to programming because there is always someone with whom to share the stress and joy of success 
    - It **enhances** team **communication** because the partners exchange ideas during pair programming 
    - Pair-switching helps team members gain insight into the components of the system. This improves productivity and quality 
    - It **enhances** mutual **understanding** and **collaboration** because pair programming lets the team members understand each other and learn from each other in various ways and aspects 
    - It tends to **produce** **simpler** and more **efficient** solutions faster because discussions stimulate creative thinking and help in problem solving

11. Pair Programming Limitations
    - It is **not for everyone**—there are programmers who prefer to work alone
    - Discussions between the partners can **take** a lot of **time**. Therefore, it should focus on solving the problem and getting the work done
    - It could be slow to start due to the need to **adapt** to the differences of the partners 
    - Other limitations: 
        - Partners need to be at the same **location**
        - It may be difficult for the partners to find a meeting **time** due to conflicting schedules
        - It might not be as effective as systematic review methods in detecting **defects**

12. Test Driven Development
    - Conventional approach to code writing  writes the program before writing the **test** cases 
    - TDD require programmer to write the **test** first, then implement the functionality (the code) 
    - Some statistics/reports indicate that: 
        - TDD can significantly **reduce defect** densities of code compared to other methods 
        - Programmer **productivity increases** due to TDD 
    - With TDD, features of a class (methods) are **implemented** and **tested** incrementally 
        - TDD fits well with **agile** methods
    - <img src="img/Test Driven Development.png" alt="Test Driven Development" width="500" height="600">

13. Merits of Test Driven Development
    - TDD helps the team understand and improve **requirements** (functionality and testability) 
    - TDD produces high-quality **code** (team constantly integrates) 
    - TDD improves program **structure** and **readability** through refactoring 
    - TDD makes it easier to **debug**

14. Potential Issues with TDD
    - Test cases may not be built to exercise full range of required functionally 
    - Test cases may not provide tests for code **robustness**
        - Check for boundary conditions 
        - Check for loop errors 
        - Check for one-off errors 
        - Check for input type and range, out of range 
        - Etc.
    - Test cases are programs and need to be written and checked in accordance with project’s coding **standards** 
        - Refactoring should include refactoring of these test scripts

15. Applying Agile Principles
    - Develop small, **incremental** releases and iterate 
        - Focus on frequent delivery of software products 
    - Complete each feature before moving onto the next 
    - Test early and often 
    - Everybody owns the code 
        - Everybody is responsible for the **quality** of the code

16. Implementation Considerations
    - Everyone in the team should follow the same coding/programming **standards** 
        - Helps produce higher quality **code** 
        - Helps to produce more **maintainable** code providing guidance to programming team of requirements and options, with examples and rationale 
    - Organization of process implementation artifacts 
        - Designs 
        - Code 
    - Converting designs to programs 
        - Work teams 
        - Pair programming 
        - Test driven development

17. Software Quality Assurance
    - Software quality assurance encompasses a set of activities to ensure that the software under development or modification will meet functional and quality requirements 
    - Software quality assurance activities are life-cycle activities

18. What is Software Quality Assurance?
    - SQA is a set of activities to ensure that the software product and/or process confirms to established **requirements** and **standards**
    - The activities include: 
        - Verification: Are we building the product right? 
        - Validation: Are we building the right product? 
        - Technical reviews 
        - Testing: Attempts to uncover program errors

19. Cost of Quality
    - Cost of quality consists of
        - Costs of **preventing** software failures 
            - Costs to **implement** an SQA framework (one time) 
            - Costs to **perform** SQA activities (on going) 
            - Costs to **monitor** and **improve** the framework (on going) 
            - Costs of needed **equipment** (machines and tools)
        - Costs of failure 
            - Costs to **analyze** and **remove** failures 
            - Costs of **lost productivity** (developer and customer) 
            - Costs of **liability**, **lawsuit**, and increased **insurance** premium 
            - Costs associated with **damage** to company’s **reputation**

20. Cost of Quality
    - Software engineering considers costs to accomplish something. Graph shows the ‘Quality’ and ‘Cost’ trade-off
    - <img src="img/Cost of Quality.png" alt="Cost of Quality" width="500" height="600">
    - <img src="img/Cost of Quality Bar.png" alt="Cost of Quality Bar" width="500" height="600">

21. Software Quality Attributes
    - Reliability 
        - adequacy: correctness, completeness, consistency; robustness 
    - Testability and Maintainability 
        - understandability: modularity, conciseness, preciseness, unambiguity, readability, measurability, assessability, quantifiability 
    - Usability 
    - Efficiency 
    - Portability 
    - etc.

22. Quality Measurements and Metrics
    - Software measurements are objective, quantitative assessments of software attributes 
    - Metrics are standard **measurements** 
    - Software metrics are standard **measurements** of software **attributes** 
    - Software quality metrics are standard **measurements** of software **quality** 
    - Class discussion: why do we need software metrics?

23. Typical Software Quality Metrics
    - Requirements metrics 
    - Design metrics 
    - Implementation metrics 
    - System metrics 
    - Object-oriented software quality metrics

24. Requirements Metrics
    - <img src="img/Requirements Metrics.png" alt="Requirements Metrics" width="500" height="600">
    - <img src="img/Requirements Metrics Q.png" alt="Requirements Metrics Q" width="500" height="600">

25. Design Metric Fan-In
    - Number of **incoming** messages or control flows into a module 
        - Measures the **dependencies** on the module 
    - High fan-in signifies a “god module” or “god class” 
        - The module may have been **assigned** too many responsibilities 
    - It may indicate a low cohesion
    - <img src="img/Design Metric Fan-In.png" alt="Design Metric Fan-In" width="500" height="600">

26. Design Quality Metrics Fan-Out
    - Number of **outgoing** messages of a module 
        - Measures the **dependencies** of this module on other modules 
    - High fan-out means it will be difficult to **reuse** the module because the other modules must also be **reused**
    - <img src="img/Design Quality Metrics Fan-Out.png" alt="Design Quality Metrics Fan-Out" width="500" height="600">

27. Modularity
    - Modularity – Measured by **cohesion** and **coupling** metrics
    - <img src="img/Modularity.png" alt="Modularity" width="500" height="600">

28. Cohesion
    - Each module implements **one and only one** functionality 
    - Ranges from the **lowest** coincidental cohesion to the **highest** functional cohesion 
    - High cohesion enhances **understanding**, **reuse**, and **maintainability**

29. Coupling
    - An interface mechanism used to **relate modules**
    - It measures the **degree** of dependency 
    - It ranges from **low** data coupling to **high** content coupling 
    - High coupling increases uncertainty of **run time effect** 
        - also makes it difficult to **test**, **reuse**, **maintain**, and **change** the modules

30. Module Design Complexity (mdc)
    - The number of integration tests required to integrate a module with its subordinate modules  
    - mdc is equal to the number of decisions to call a subordinate module (d) plus one
    - <img src="img/Module Design Complexity.png" alt="Module Design Complexity" width="500" height="600">

31. Design Complexity S0
    - Number of  subtrees in the structure chart with module M as the root 
    - S0(leaf) = 1 
    - S0(M) = mdc+S0(child1 of M)+...+S0(childn of M)
    - <img src="img/Design Complexity S0.png" alt="Design Complexity S0" width="500" height="600">

32. Integration Complexity
    - Minimal number of integration test cases required to integrate the modules of a structure chart
    - Integration complexity S1 = S0 – n + 1, where n is the number of modules in the structure chart

33. Implementation Metrics
    - Defects/KLOC 
    - Pages of documentation/KLOC 
    - Number of comment lines/LOC 
    - Cyclomatic complexity 
        - equals to number of binary predicates + 1 
        - measures the difficulty to comprehend a function/module 
        - measures the number of test cases needed to cover all independent paths (basis paths)

34. Object-Oriented Quality Metrics
    - Weighted Methods per Class (WMC)
        - WMC(C) = Cm1+ Cm2+ ··· + Cmn where: Cmi=complexity metrics of methods of C 
        - Significance: Time and effort required to understand, test, and maintain class C increases exponentially with WMC
    - Depth of Inheritance Tree (DIT)
        - Distance from a derived class to the root class in the inheritance hierarchy 
        - Measures . . . 
            - the degree of **reuse** through inheritance 
            - the difficulty to **predict** the behavior of a class 
            - costs associated with **regression** testing due to change impact of a parent class to descendant classes
        - High DIT Means Hard to Predict Behavior
            - All three classes include print()
            - It is difficult to determine which “print()” is used:
            - Change to parent class requires retesting of subclasses
            - <img src="img/High DIT Means Hard to Predict Behavior.png" alt="High DIT Means Hard to Predict Behavior" width="500" height="600">
            - <img src="img/High DIT Means Hard to Predict Behavior Subclass.png" alt="High DIT Means Hard to Predict Behavior Subclass" width="500" height="600">
    - Number of Children (NOC)
        - NOC(C) = |  C’ : C’ is an immediate child of C | 
        - The dependencies of child classes on class C increases proportionally with NOC 
        - Increase in dependencies increases the change impact, and behavior impact of C on its child classes 
        - These make the program more difficult to understand, test, and maintain
    - Coupling Between Object Classes (CBO)
        - CBO(C) = | C’ : C depends on C’ | 
        - The higher the CBO for class C the more difficult to understand, test, maintain, and reuse class C
    - Response for a Class (RFC)
        - RFC (C) = | m : m is a method of C, or m is called by a method of C | 
        - The higher the RFC, the more difficult to understand, test, maintain, and reuse the class due to higher dependencies of the class on other classes
    - Lack of Cohesion in Methods (LCOM)
        - LCOM (C) = n * (n-1) / 2 - 2 * | (mi, mj) : miand mjshare an attribute of C | 
        - LCOM measures the number of pairs of methods of C that do not share a common attribute 
        - Class exercise: 
            - Is it possible to derive a metric called “cohesion of methods of a class?” 
            - If so, what would be the formula?

35. Reliability and Availability
    - Mean time to failure (MTTF)
    - Mean time to repair (MTTR)
    - Mean time between failure (MTBF) = MTTF + MTTR
    - Availability = MTTF/MTBF X 100%
    - <img src="img/Reliability and Availability.png" alt="Reliability and Availability" width="500" height="600">

36. Usefulness of Quality Metrics
    - Definition and use of indicators 
    - Directing valuable resources to critical areas 
    - Quantitative comparison of similar projects and systems 
    - Quantitative assessment of improvement including process improvement 
    - Quantitative assessment of technology

37. Software Verification and Validation Techniques
    - Desk checking
    - Inspection
        - Software Inspections
            - Inspector examines the representation with the aim of detecting **anomalies** and **defects** 
            - It does not require execution so it can be used **before** implementation 
            - It can be applied to **any** representation of the system (requirements, design, code, test data, etc.) 
            - It is an effective technique for error **detection**
        - Program Inspection
            - An approach for **detecting** defects, **not correcting** defects 
            - Defects may be **logical errors**, **anomalies** in the code that might indicate an **erroneous condition** (e.g. an uninitialized variable) or **non-compliance** with standards
            - Step by step reading the program 
            - Check against a list of criteria
                - common errors 
                - standards 
                - consistency rules
        - Inspection Pre-condition
            - A **precise** specification must be available 
            - Team members must be familiar with the organisation **standards**
            - Syntactically correct **code** must be available 
            - An error **checklist** should be prepared 
            - Management must accept that inspection will increase **costs** early in the software process 
            - Management must not use inspection for **performance** evaluation
        - Inspection Procedure
            - System overview presented to inspection team 
            - Code and associated documents are distributed to inspection team in advance 
            - Inspection takes place and discovered errors are noted 
            - Modifications are made to repair discovered errors 
            - Re-inspection may or may not be required
        - Fagan Inspection Technique
            - A checklist of **inspection** items 
            - It includes design and code **inspections** 
            - 4 - 5 **inspectors**: moderator, designer, programmer, tester 
            - It is good for **real-time** systems because errors  are not repeatable 
            - Different programmers tend to have different error **patterns**
        - Design Inspection
            - Precondition: functional  requirements and design specification must exist 
            - Design inspection checks for 
                - completeness 
                - consistency, and 
                - correctness
    - Walkthrough
        - The developer loudly reads through the program 
        - The developer provides explanation if he/she deems necessary 
        - Other team members may ask questions and stimulate doubts 
        - The developer answers questions and justifies answers
        - Difference Between Walkthrough and Inspection
            - <img src="img/Difference Between Walkthrough and Inspection.png" alt="Difference Between Walkthrough and Inspection" width="500" height="600">
    - Peer review
        - The product is reviewed by peers, guided by a list of review questions 
        - The review questions are designed to assess the product 
        - The reviewers are given one to two weeks to review the work 
        - The review results may vary due to difference in reviewer’s knowledge, experience, background, and criticality 
        - A review meeting is usually conducted to discuss the review results
        - Peer Review Procedure
            - A product overview is presented to the reviewers, who are assigned products to review 
            - The reviewers evaluate the product and answer the review questions independently 
            - At the review meeting, the reviewers present their comments and suggestions, and the developer answers questions and clarifies doubt 
            - After the review meeting, the developer fixes the problems and produces a summary of solutions 
                - The reviewers check the changes
            - A second review meeting may be needed
    - Testing
    - <img src="img/Software Verification and Validation Techniques.png" alt="Software Verification and Validation Techniques" width="500" height="600">

37. Verification and Validation in the Life Cycle
    - <img src="img/Verification and Validation in the Life Cycle.png" alt="Verification and Validation in the Life Cycle" width="500" height="600">

38. Verification
    - Verification is the process of **checking** that the “implementation” conforms to the “specification” 
    - A lower **level** abstraction is the implementation of a higher **level** abstraction 
    - Design verification involves **checking** that the software design satisfies the requirements specification

39. Validation
    - Validation aimed at **checking** the correspondence between a model and the real world, e.g., a functional specification corresponds to the real needs of the customer 
    - A **process** that seeks to refute a model 
    - Functional **testing** is a validation technique

40. Static and Dynamic Verification & Validation
    - **Review** and **inspection** are concerned with analysis of  the **static** system representation to detect problems  (static verification) 
    - **Testing**  is concerned with executing the product and observing its behaviour (**dynamic** verification)







































