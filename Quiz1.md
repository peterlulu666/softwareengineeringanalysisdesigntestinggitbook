## Quiz      

1. Question 1
  - 3 / 3 pts
  - A feasibility study is used to: C
    - ensure requirements are verifiable. 
    - determine if research and development of an idea would be beneficial to software development in the future. 
    - determine if the project is doable under the given constraints. 
    - eliminate unfeasible requirements. 
 
2. Question 2
  - 3 / 3 pts
  - Orders from customers with unpaid invoices totaling more than $1,000 must not be accepted. If this should be stated in the requirements specification, then it should be specified as: C
    - an interface requirement. 
    - a functional requirement. 
    - a non-functional requirement. 
    - a constraint. 
 
3. Question 3
  - 3 / 3 pts
  - A client requests that the system under development must not use a vendor-specific product. This should be specified in the requirements specification as: A
    - a constraint. 
    - a precondition. 
    - a functional requirement. 
    - an assumption. 
 
4. Question 4
  - 3 / 3 pts
  - The following lists the advantage of an iterative developmental process over a sequential developmental process.  Which one of the following responses is incorrect? D
    - it gets the product to the customer earlier in the developmental life cycle. 
    - it gets the software production line running efficiently earlier. 
    - it requires the customer to report problems and the team to address delivery problems earlier in the developmental life cycle. 
    - it requires less planning, coordination, communication and integration. 
 
5. Question 5
  - 3 / 3 pts
  - Which of the following is NOT correct about the Agile (Scrum) software process? B
    - it utilizes a 15-minute (short) daily stand-up meeting. 
    - it does not emphasize process. 
    - it is an iterative process. 
    - it delivers capabilities to the customer based on the customer's priorities. 
 
6. Question 6
  - 3 / 3 pts
  - Systems engineering is NOT which of the following: C
    - a process that covers the entire system life cycle. 
    - it defines the system requirements and constraints, allocates the requirements to the hardware, software, and human subsystems, and integrates these subsystems to form the system. 
    - a comprehensive, engineering bottoms-up approach to systems development. 
    - a multidisciplinary approach to systems development. 
 
7. Question 7
  - 3 / 3 pts
  - Which of the following is NOT a typical system decomposition strategy? B
    - Decompose the system according to engineering disciplines. 
    - Decompose the system according to customer specified software methodologies and processes. 
    - Decompose the system according to the functional units of the organization. 
    - Decompose the system according to system functions. 
 
8. Question 8
  - 3 / 3 pts
  - Which of the following activities identifies and describes the capabilities of the system? D
    - System Requirements Elicitation. 
    - System Deployment and Maintenance. 
    - System Architectural Design. 
    - System Requirements Definition and Specification. 
 
9. Question 9
  - 3 / 3 pts
  - Which of the following is NOT a typical architectural design diagram? D
    - Data flow diagram 
    - SysML Block diagram 
    - UML component diagram 
    - Functional diagram 
 
10. Question 10
  - 3 / 3 pts
  - Indicate the best answer of the following: D
    - System engineering is about analysis and design, while software engineering is about programming. 
    - Software engineering is a broad area that includes system engineering and other disciplines. 
    - Software engineering and system engineering are interchangeable. 
    - Systems development involves many disciplines including software engineering. 
 
11. Question 11
  - 3 / 3 pts
  - Which of the following activities defines the solution or ‘how’ to provide the system capabilities? A
    - System Architectural Design. 
    - System Requirements Definition and Specification. 
    - System Deployment and Maintenance. 
    - System Requirements Elicitation. 
 
12. Question 12
  - 3 / 3 pts
  - Which of the following is NOT a typical system architectural design consideration? A
    - The correctness of the requirements elicitation and specification. 
    - A robustness and reliability of the system. 
    - The mix of hardware, software, and firmware used. 
    - The independence and interrelation of the system. 
 
13. Question 13
  - 3 / 3 pts
  - Factors that are considered when allocating system requirements to subsystems include: A
    - developmental and operational costs, system performance, and ease of system maintenance. 
    - applicable programming languages and operating systems to implement the subsystems. 
    - communications bandwidth, processor technologies, and hardware/software trade-offs. 
    - hardware, firmware, and software components that might implement these requirements. 
 
14. Question 14
  - 3 / 3 pts
  - Which of the following activities discovers the capabilities of the system? A
    - System Requirements Elicitation. 
    - System Architectural Design. 
    - System Requirements Definition and Specification. 
    - System Deployment and Maintenance. 
 
15. Question 15
  - 3 / 3 pts
  - Validation is: C
    - typically performed by Software Quality Engineering personnel. 
    - building the product right. 
    - building the right product. 
    - the same as dynamic testing. 
 
16. Question 16
  - 3 / 3 pts
  - Software quality assurance: C
    - are quality checkpoints performed by independent personnel. 
    - are process audits meant to determine process compliance. 
    - ensures that the system will satisfy requirements and quality standards. 
    - is typically independent testing. 
 
17. Question 17
  - 3 / 3 pts
  - Verification is: B
    - building the right product. 
    - building the product right. 
    - dynamic testing. 
    - typically performed by Software Quality Engineering personnel. 
 
18. Question 18
  - 3 / 3 pts
  - Risk management is: D
    - an SQA activity. 
    - an activity of the Unified Process. 
    - a dynamic validation activity. 
    - a project management activity. 
 
19. Question 19
  - 3 / 3 pts
  - Object-oriented software engineering is: A
    - a specialization of software engineering. 
    - an engineering approach to software programming. 
    - design in UML and program in an OO language. 
    - Java or C++ program development. 
 
20. Question 20
  - 3 / 3 pts
  - Which of the following are typical project management activities? B
    - dynamic testing. 
    - planning and scheduling. 
    - quality control and assurance. 
    - information systems management. 
 
21. Question 21
  - 3 / 3 pts
  - Software engineering aims to: C
    - produce a software system and deliver it on time. 
    - make sure that the software system functions correctly. 
    - significantly improve software productivity and software quality while reducing software costs and time to market. 
    - apply computer science techniques to build software systems. 
 
22. Question 22
  - 3 / 3 pts
  - What are the three tracks of software engineering life cycle activities? A
    - Development process, quality assurance, and project management. 
    - Development, testing, and maintenance. 
    - Analysis and design, program construction, and quality assurance. 
    - Analysis and design, implementation, and testing. 
 
23. Question 23
  - 3 / 3 pts
  - A software methodology: C
    - is a systematic approach to software engineering. 
    - describes the what and where of the software activities. 
    - describes the detailed steps and how to perform the activities of a software process. 
    - specifies the framework of activities and when to perform them. 
 
24. Question 24
  - 3 / 3 pts
  - The software process used by the agile unified methodology presented in the textbook adopts: AD
    - the waterfall process. 
    - a combination of waterfall and agile processes. 
    - a traditional software development methodology. 
    - an iterative process. 
 
25. Question 25
  - 3 / 3 pts
  - A software process (or SDLC): A
    - describes the detailed steps and how to perform the activities of a software process. 
    - is a systematic approach to software engineering. 
    - specifies the ‘when’ to perform the software activities. 
    - describes the artifacts produced for each of the software activities 
 
26. Question 26
  - 3 / 3 pts
  - Which of the following is NOT a property of a wicked problem? C
    - The solution cannot be separated from the specification. 
    - Each step of the problem-solving process has an infinite number of choices. 
    - The problem can be formulated definitely and completely. 
    - The solution is subject to life-long testing. 
 
27. Question 27
  - 3 / 3 pts
  - Which one of the following is NOT a characteristic of the traditional understanding of the waterfall process? B
    - that it delays customer delivery until the very end of the SDLC. 
    - that it is quick to respond to requirements change. 
    - that it is predominantly a single pass through the SDLC. 
    - that it emphasizes requirements correctness at the beginning of the SDLC. 
 
28. Question 28
  - 3 / 3 pts
  - Peer review, walkthrough, and inspection are types of: B 
    - software verification. 
    - technical reviews. 
    - customer review. 
    - expert review. 
 
29. Question 29
  - 3 / 3 pts
  - In a requirements specification, constraints: C
    - are semantic rules that govern the operation of the system. 
    - are requirements specifying restrictions on the input parameters. 
    - specify restrictions on the design and implementation space. 
    - specify acceptable values of variables used or produced by the software system. 
 
30. Question 30
  - 4 / 4 pts
  - Frederick Brooks pointed out that the hardest single part of building a software system is: D
    - developing efficient algorithms to solve the right problem. 
    - how to prove that the requirements are correct. 
    - how to produce a good architectural design. 
    - deciding precisely what to build. 
 
31. Question 31
  - 3 / 3 pts
  - A client requests that the system under development must use a specific programming language. This should be specified in the requirements specification as: A
    - a constraint. 
    - a precondition. 
    - a nonfunctional requirement. 
    - a functional requirement. 
 
32. Question 32
  - 3 / 3 pts
  - Analysis models are constructed: D
    - to serve as design documents to facilitate implementation and testing. 
    - to show how the classes and processes are related to each other. 
    - to provide comprehensive documentation to improve quality. 
    - to help understand an application and requirements. 
 
33. Question 33
  - 3 / 3 pts
  - Requirements are: A
    - items that are important to the customer. 
    - capabilities that the system must deliver. 
    - items captured in a software requirements specification (SRS). 
    - identifications of system features. 


