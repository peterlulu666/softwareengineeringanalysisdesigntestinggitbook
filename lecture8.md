## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1okus58outoMJzLXF2cvfW-RHsv-EaRR3/preview"></iframe>

## Quiz        

1. A use case is a business process.
    - Begins with an actor.
    - Ends with the actor.
    - Accomplishes a business task for the actor.

2. The use cases are **derived** from requirements and they must **satisfy** the requirements.

3. Plan the development and deployment of use cases and subsystems to meet the customer’s business needs and agreed-to priorities.

4. The use case.
    - A use case is a business process.
    - A use case must be started by an actor.
    - A use case must end with the actor.
        - The actor explicitly or implicitly acknowledges the accomplishment of the business task.
    - A use case must accomplish a business task for the actor.

5. The actor.
    - An actor represents a business role played by (and on behalf of) a set of business entities or stakeholders.
    - Actors are not part of the system. – Actors interact with the system.
    - Actors are often human beings but can also be a piece of hardware, a system, or another component of the system.
    - Actors initiate use cases which accomplish business tasks for the respective actors.

6. The use case diagram
    - The system name.
    - The actor.
    - The association.
    - The system boundary.
    - The use case.

7. Inheritance.
    - It states that an Admin “IS-A” Patron.

8. Avoid showing.
    - Many use cases in one diagram (see next slide). 
    - Many use case diagrams each containing only one use case.
    - Overly complex use case diagrams. 
    - Unnecessary relationships between use cases.

9. The use several diagrams to show groups of closely-related use cases.
    - Show only use cases and actors that are relevant.
    - Provide a meaningful name for the system/subsystem that implements a group or sub-group of use cases.

10. The guideline for the use case diagram. 
    - Use inheritance to simplify the diagram by reducing the number of actor-use case links.
    - Give a meaningful name for the system/subsystem that contains the use cases.
        - The name may serve as the package or module name in design/implementation.
    - Actor-use case relationships are always association relationships.
    - Only use cases and their relationships can be shown within the system boundary.






























