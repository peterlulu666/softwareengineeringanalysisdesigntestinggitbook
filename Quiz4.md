## Quiz      

1. The best reason to model the states in a system using a state diagram is
which ONE of the following?
    - The system produces actions that are similar to state devices. It seems like a good approach to use - no other approaches are examined.
    - The system is a real-time system; all real-time systems use states to model system behavior.
    - **The system is driven by external events or causes external events. Only the states that are driven by external stimuli are captured.**
    - The system seems to have natural states, values, or enumerations. A state diagram could be a candidate - no other approaches are needed.

2. In a state diagram, a state that has no outgoing transitions is
referred to as:
    - an unreachable state.
    - a leaf state.
    - a destination state.
    - **a dead state.**

3. A valid state diagram must always have which of the following?
    -   state variables.
    - an end state.
    - **a start state and all inputs/outputs labeled.**
    - a corresponding state table.

4. In a state diagram, a transition with a guard condition means . . .
    -   if the transition condition is true, then the guard is fired.
    - **the transition is fired whenever the guard condition is evaluated to true.**
    - if the transition is fired, then the guard condition becomes true.
    - the guard can be fired only if the transition condition is evaluated to true.

5. In a state diagram, a state that has no in-going transitions is referred to as . . .
    -   a dead state.
    - a leaf state.
    - **an unreachable state.**
    - **a destination state.**

6. In an activity diagram, a bar with multiple in-going arrow lines represents . . .
    -   conditional branching.
    - **synchronization.**
    - forking.
    - conjunction.

7. Decision-making structures in the code are best modeled by which of the following in an activity diagram?
    - The use of forks and joins.
    - **The use of branching and merges.**
    - The use of object and control flows.
    - The use of final and final flow nodes.

8.  Tasking structures in the code are best modeled by which of the following?
    - The use of object and control flows.
    - The use of final and final flow nodes.
    - **The use of forks and joins.**
    - The use of branching and merges.

9. Activities coordinated across multiple subsystems can be represented in an activity diagram using . . .
    - a coordination lane.
    - **a swim lane.**
    - a task lane.
    - a workflow diagram.

10. Black box testing refers to . . .
    - understanding program structure and verifying all def-use paths through the code.
    - understanding test coverage and establishing minimum test coverage requirements for the system and test team.
    - **understanding the functional specification of the system, focusing on testing requirements.**
    - understanding the internal workings of the system, focusing on testing the program logic and structure.

11. Coding standards are . . .
    - a set of documentation rules.
    - required by all software development organizations.
    - defined to measure the quality of code.
    - **industry-wide standards for writing code.**

12. Which of the following are good test items to consider adopting in programming standards?
    - Rules limiting the length of variable names.
    - **Limitations (or restrictions) on the use of exception handling in the code.**
    - Code formatting rules to provide a similar look to the code.
    - Contents and structure of manually generated version numbers.

13. Which of the following is NOT a typical characteristic of an Agile team?
    -   Continuous communications and interaction.
    - Strong customer collaboration.
    - Responding to and embracing change.
    - Working software over documentation.
    - **Dynamic lead programmer approach.**

14. The agile manifesto contains 12 principles that represent the core agile values. Select the item below that is NOT one of these 12 principles.
    - Business people and developers must work together daily throughout the project.
    - **Monitor development and promote individuals to more senior position as successful experience grows.**
    - Working software is the primary measure of progress.
    - Welcome changing requirements, even late in development.
    - Continuous attention to technical excellence and good design enhances agility.

15. A sprint in Agile represents which of the following?
    -  An iteration backlog item that requires immediate correction.
    - A release typically tied to a customer event or milestone.
    - A product backlog item that requires immediate correction.
    - **A recurring iteration of a fixed length.**
    - The set of highest priority items to be addressed from the product backlog.

16. The difference between ‘Iteration planning’ and ‘Release planning’ is best described by which of the following?
    - An iteration is not planned but the release is.
    - **An iteration is a recurring fixed length event in a release plan.**
    - An iteration plans and defines the product backlog while the release plans the number of stories to be handled.
    - A release is a customer event at the end of the life cycle.
    - A release is not planned but is the result of a workable iteration.

17. Which of the following is NOT one of the questions typically asked in a daily Agile stand-up meeting?
    - **Why are you behind and how are you going to catch up?**
    - What things are blocking you?
    - What have you completed since the last meeting?
    - What are you going to get done today?

18. Verification, validation, and testing . . .
    - are design techniques used by software engineers.
    - **are life cycle activities.**
    - are activities performed by software project management.
    - are customer activities.

19. Peer review, walkthrough, and formal inspection are types of . . .
    - **technical reviews.**
    - customer review.
    - expert review.
    - software validations.

20. A technical review of the code is . . .
    - a dynamic test.
    - an examination.
    - **a verification activity.**
    - a static test.
    - a validation activity.

21. The word ‘bug’ is synonymous with which of the following words?
    - mistake.
    - failure.
    - fault.
    - defect.
    - error.

22. ‘Debugging’ is the process of . . .
    -   determining the root cause of identified software problems.
    - finding problems in the software.
    - performing technical reviews of software artifacts.
    - **fixing problems in the software.**

23. The term ‘failure’ is best defined as which of the following?
    - a bug or defect.
    - **a deviation of the component or system from its expected delivery, service, or result.**
    - the behavior predicted by the specification or another source of the component or system under specified conditions.
    - the result of software performance degrading over time.

24. For the relationship between months and days in a month (ignoring leap years), how many equivalence classes are there?
    - 5
    - 2
    - **3**
    - 4
    - 1

25. The phrase ‘test oracle’ is best defined as which of the following?
    - **a source to determine the expected results of the software under test.**
    - a program used to automatically develop test cases.
    - an expert in test.
    - a program used to determine unreachable conditions in the code.

26. Which of the following best fills in the blank in the following statement?  “A _________ is a Boolean expression composed of conditions and zero or more Boolean operators.”
    - **decision**
    - conjunction
    - derivation
    - condition

27. The term ‘verification’ is best accomplished by which of the following activities?
    - modeling or prototyping software requirements.
    - **executing software tests.**
    - performing expert reviews.
    - performing customer reviews.

28. The term ‘validation’ is best accomplished by which of the following activities?
    - performing code inspections.
    - reusing planning documents from other programs.
    - **modeling or prototyping software requirements.**
    - performing unit tests.

29. What is the cyclomatic complexity of the program in the following figure?
    - 5
    - 6
    - 4
    - **3**
    - 1

30. Which of the following is true?
    - Software testing can prove there are no defects in a program.
    - If no errors are found by software tests, then the software must be error-free.
    - **Software testing intendeds to find as many defects as possible.**
    - Automation can help software test every feature to ensure no bugs.

31. If the value range of a variable is specified as (a, b), then testing the component using values a, b, above a, above b, below a, and below b is an application of . . .
    - exception testing
    - **boundary value testing**
    - condition testing
    - extreme testing

32. Which two specification-based techniques are most closely related to each other?
    - decision tables and boundary value analysis.
    - decision tables and state transition testing.
    - **equivalence partitioning and boundary value analysis.**
    - equivalence partitioning and state transition testing.

33. White box testing refers to . . .
    - understanding the functional specification of the system, focusing on testing requirements.
    - understanding test coverage and establishing minimum test coverage requirements for the system and test team.
    - **understanding the internal workings of the system, focusing on testing the program logic and structure.**
    - understanding program structure and verifying all def-use paths through the code.


 
 
 
 
 
 
 
 
 
 
 
 
 
