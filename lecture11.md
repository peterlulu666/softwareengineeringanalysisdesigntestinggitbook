## Quiz      

1. Design by Contract 
    - Design by contract, is an approach for designing software that prescribes that software designers should define formal, precise, and verifiable interface specifications for software components which extend the ordinary definition of abstract data types with pre-conditions, post-conditions, and invariants. 
    - These specifications are referred to as "contracts" in accordance with a conceptual metaphor with the conditions and obligations of legal contracts. 
        - The contract is fulfilled through the use of pre-conditions and post-conditions. 
    - A pre-condition is a condition or predicate that must always be true just before an operation in a formal specification. It defines any constraints on object state which are necessary for successful execution. This constitutes the actors portion of the contract. 
    - The actor is obligated to ensure that the precondition holds prior to calling the routine. The reward for the caller's effort is expressed in the called routine's post-condition. 
    - A post-condition is a condition or predicate that must always be true just after an operation in a formal specification. 
    - Example of pre-conditions and post-conditions 
        - Square root function 
            - Pre-condition: the input must be non-negative 
            - Post-conditions: 
                - 1. The output will be a the square root of the input within a given accuracy and precision. 
                - 2. The output squared will be equal to the input (given the accuracy specifications). 
    - Although programming languages have facilities to make assertions like these, Design by Contract (DBC) adherents consider these contracts to be so crucial to software correctness that they should be accomplished as part of the design process. 
    - In effect, DBC advocates writing the assertions first. 

2. User Interface Prototypes
    - "A picture is worth a thousand words." -- Fred R Barnard 
    - Showing user interface prototypes with expanded use case specification is extremely useful to obtain user feedback early in the software development lifecycle. 
    - These user interface prototypes may be reused in the preparation of the user's manual, which can take place concurrently with the development effort. 
    - So, what is a User Interface Prototype? 
        - A picture of the user interface in action 
        - In our case (for our projects) the UIP will be a drawing of the phone screen and what it might look like illustrating a specific operation. 

3. The UI prototype always shown only on the system side. 

4. Guidelines for Expanded Use Case 
    - Expanded use cases inherit all the guidelines for high level use cases 
        - Because expanded use cases are refinements of high level use cases 
    - Expanded use case specifications should state a user interface that a novice actor can easily begin 
        - Example: "0) System displays the homepage." 
    - Expanded use cases may show prototypes of the user interfaces 
        - For soliciting feedback from the customer/user 
        - To inform the programmer what the Ul should look like 
    - Expanded use case specifications should always end with the actor, preferably with an actor action to acknowledge that the system has accomplished the task properly. 
    - Expanded use case specifications should include pre- and post-conditions to explicitly specify the assumptions and effects of the use case. 

5. Preparing for Design Patterns 
    - In the next Module (M08) we will study sequence diagrams 
    - But in order to avoid some re-work we want to start thinking about these terms in the expanded use cases before we apply design patterns to them (M09) 
    - These terms will help us make the jump to the design patterns much more easily 
    - Start to think of each Expanded Use Case utilizing the following three components 
        - A GUI (e.g. Add Program GUI) 
        - A Controller (e.g., Add Program Controller) 
        - A Database Manager (e.g., DBMgr) 
    - Each Exp UC will have its own GUI, Controller, and DBMgr which later can be combined with other EUCs, if necessary 
    - When we develop sequence diagrams, we will use these components for each Exp UC 
        - M09 Design Patterns will explain why these 3 components are important 

6. Expanded Use Case Summary 
    - We have learned three important principles: 
        - 1. How to develop an Expanded Use Case 
            - a. Always starts with the System displaying some information (status/message/web page) as Step 0 
            - b. Step 1 picks up the TUCBW 
            - c. Always ends on an actor step with confirmation that the actor sees/acknowledges the use case has provided the desired response. This is the TUCBW 
            - d. The EUC always has a pre-condition and post-condition 
    - 2 Each Expanded Use Case will need to have at least one User Interface Prototype (UIP) 
        - a. They are always shown only on the "System Side" of the EUC 
        - b. These UIPs help to fill in missing details from the requirements 
    - 3. Each EUC will have in the Sequence Diagrams the following three components 
        - a. UC GUI 
        - b. UC Controller 
        - c. DB Mgr 

7. Expanded Use Grading Criteria 
    - 1. Does each EUC have a correct actor name and system name? Are the actor names consistent with the UCD? 
    - 2. Are there pre-conditions and post-conditions for each EUC? 
    - 3. Are there TUCBW/TUCEW for each EUC? Are they exactly the same for the HL UCs? 
    - 4. Does each EUC start at Step 0 (a system step)? Do they end on an actor step? 
    - 5. Is each step numbered? 
    - 6. Are there no blank entries in the use case interactions? 
    - 7. Does each actor step clearly and correctly specify the actor input and actions? 
    - 8. Does each system step clearly and correctly specify the system responses? 
    - 9. Does each EUC have a name? Is this name the same as the use case for which it is expanded? 
    - 10. UI Prototypes 
        - a. Does each EUC have at least one UIP? 
        - b. Are the UIPs only shown on System steps? 
        - c. Are additional UIPs used for system responses where needed? 
            - This is needed when the user is inputting information (attributes) 
        - d. Does the system step provide a clear link to the UIP? 
        - e. Do the attributes from these UIPs match attributes in the domain diagram and in the requirements? 









