## Quiz      

1. The waterfall process defines a straight sequence of activities.      
    - The merit, 
        1. simplifie project planning. 
        2. allow function oriented project organization. 
        3. good for large and complex system.
    - The drawback, 
        1. difficult to respond to requirement change. 
        2. the user cannot experiment with the system to provide feedback until it is released. 
        3. the risk of low return.      

2. The V model
    - The left side is the requirement.
    - The right side is the test.      

3. The tame problem, the waterfall process is for tame problem
    - The chess     
    - The math
    - The operations research
    - The compiler construction
    - OS
    - AI

4. The SE is not the tame problem
    - The number of steps is the finite. This is not for SE.
    - The SE is not the scientific process.      

5. The software development is the wicked problem
    - There is no definite formulation.      
    - The specification and solution cannot be seperated.
    - There is no stopping rule.       
    - The solution can only be good and bad.      

6. The wicked problem 
    - The urban planning.
    - The National policy making. 
        - They always compromise. There is no simple solution.      
    - The economic reform.
    - The application software development.

7. The waterfall process is for tame problem. The software development is not the tame problem. The software development is the wicked problem. There is better process.       

7. The software process model
    - The prototyping process, assess time and budget constraint.
    - The throwaway prototype, a lot of effort is wasted.
    - The evolutionary prototype, intelligent system, research software, and embedded system.

8. The spiral process
    - The risk management.      
    - The system evolves incrementally as the model iterates the spiral cycles.        

9. The unified process
    - There is four interation.      
    - Every iteration include requirement analysis, design, implementation, and testing.      
    - Every cycle include multiple interation.      

10. The personal software process
    - Train individual software engineer.      
    - The script, form, standard, and guideline.    
    - It help you to identify areas for improvement.       
    - It prepares software engineer for the team project.       

11. The team software process

12. The agile process
    - The waterfall works well for tame.      
    - The agile process is designed to solve the wicked problem.  
        - Emphasize teamwork
        - Emphasize joint development
        - Rapid development
        - Verification
    - It is guided by agile values, principles, and best practices.      
    - It encourage user involvement.     

13. The agile value
    - individual and interaction
        - The software development is intellectual. If they do not know design, if they do not communicate, then the result will not be good. So individual's interact, involvement, and contribute is essential to the success of the project.     
    - working software
        - The documentation and working software.
    - customer collaboration
        - The quick change contract, contract negotiation, communication, and mutual understanding.
    - responding to change
        - The change control process and plan.        

14. The agile principle

15. The agile process model
    - The requirement analysis
    - The design
    - The implementation
    - The testing
    - The deployment

16. The software process
    - The activities.
    - What and when to be done in every phase.
    - The paradigm independent.
    - A phase realized by methodology.

17. The software methodology, waterfall process, unified process
    - The detailed step.
    - How to carry out the activities of a process.      
    - The paradigm dependent.
    - It is the cook book. Every step describe procedure, technique, and guideline.
    - A phase of a process.      

17. The software paradigm
    - The procedural paradigm, building blocks and basic unit is process.
    - The OO paradigm, building bloacks and basic unit is object.
    - The data oriented paradigm, building blocks and basic unit is data and relationship.


18. The benefit of methodology
    - Perform better on task.
    - Improve communication and productivity.
    - Improve quality.

19. The dynamic system development method, DSDM
    - The lifecycle activities, study, functional, design and build, and implementation.        

20. The feature driven development, FDD
    - The lifecycle activities, develop domel, build feature, plan feature, design feature.      

20. The scrum
    - The lifecycle activities, plan meeting, sprint iteration, sprint review meeting, and deployment.        

20. The extreme programming, XP
    - It is before agile process.        

20. The crystal clear

21. The lean development



































