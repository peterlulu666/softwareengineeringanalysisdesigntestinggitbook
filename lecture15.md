## Lecture        

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1C3UfurW3kMDO379tpAMeB-2QLpyCOB7b/preview"></iframe>

## Quiz        

1. Step 3-specifying Interaction Behavior
    - In this step the interactive **behavior** of the user interface is designed and specified using a state diagram
        - The states of the diagram represent the windows and dialog boxes
        - The transitions (events) represent the user input and user actions that cause the change from one window to dialog box to another
    - The states are annotated with the windows and dialog boxes they display, as shown on the next slide
        - This helps the designer visualize the transitions from one display to another
        - This helps to ensure that allowable options from one state to the next are well thought out
    - State Diagram Editor Behavior
    - <img src="img/State Diagram Editor Behavior.png" alt="State Diagram Editor Behavior" width="500" height="600">

2. Step 4-construct a Prototype
    - Types of prototypes
        - Static approaches generate non-executable **prototypes**
            - Here the layout design is depicted on paper or in a slide show tool (e.g., PowerPoint) or a word processing tool
            - This is effective in showing the ‘look’ of individual windows and widgets but does not convey the transition between these very well (behavioral)
        - Dynamic approaches generate executable prototypes using a **prototyping** tool
            - The prototype can range from simple to fully implemented behavior
            - This is effective in not only showing the look of individual windows and widgets but also the ‘feel’ (i.e., behavior)
            - A full behavioral approach can take considerable effort to develop, but if the prototype is usable in the deliverable product then it has many advantages going forward
        - Hybrid approaches
            - Construct static prototypes during the initial stage of prototype development and switch to dynamic prototyping later after the users are satisfied with the ‘look’ and ‘feel’ of the prototypes

3. Step 5 -Evaluate User Interfaces with Users
    - User Interface is the first thing that a user sees
        - UI does a lot to sell users on the quality of the software
        - Engage with the user as soon as possible to nail down the UI aspect of the software (user backgrounds, opinions, past experiences, other software)
    - Here are some of the approaches used to **evaluate** the interface design with the user
        - User interface presentation
            - This is performed in **the initial stages** where the developer shows the windows, widgets, dialog boxes while tracing transitions of the state diagram
            - This process is **iterative** and uses **a depth-first traversal** until all transitions are traversed
        - User interface demonstration
            - Static and/or dynamic prototypes are used
        - User interface experiment
            - Here the developer demonstrates the prototype and lets the user experiment with it for a short period
        - User interface review meeting
            - This approach involves user interface design experts, domain experts, user representatives, and developers
        - User interface survey
            - This approach uses a survey to solicit feedback from the users, user domain experts, and user interface design experts

4. User Interface Design Review checklist
    - 1. Is the overall user interface design consistent with the standard user interface design in each window? 
    - 2. Does each window, dialog box, and widget have a simple appearance to allow the user to focus on the main theme of interaction? 
    - 3. Is the user interface behavior consistent  with the behavior specified in the expanded use case? 
    - 4. Does the user interface design make the system easy to learn and use? 
    - 5. Are the GUI widgets used correctly and descriptive texts precise and informative from a new user standpoint? 
    - 6. Are the labeling of the GUI widgets and descriptive texts precise and informative from a new user standpoint? 
    - 7. Are the icons used properly and informative of their function? 
    - 8. Is the system output logically and properly structured and organized to facilitate understanding? 
    - 9. Are the messages clear and clearly displayed? 
    - 10. Are the error messages free of implementation detail, informative, and helpful to the user? 
    - 11. Are color, blinking, and other user interface techniques used carefully, properly, and in a restrictive manner to avoid distraction?

5. User Support Capabilities
    - User support capabilities include **online documentation, context-dependent help, error messages, and recovery**
        - Online help should let the user find the needed information easily
        - Context-dependent help is a user-friendly design technique
            - Chain of responsibility supports this
        - **Error messages** should be **user-oriented**, rather than developer- oriented, and be **easy** to understand
            - “illegal operation in line 75 of . . .” -- developer error message 
                - This does nothing for the user – can stop, slowdown, or confuse user 
                    - Doesn’t suggest remedial action 
                    - Doesn’t indicate that data or process is lost
            - Want user to recover from an undesired/error state of the system
                - Undo 
                - Auto-recovery of data 
                - Fault tolerance design

6. Tool Support








        
