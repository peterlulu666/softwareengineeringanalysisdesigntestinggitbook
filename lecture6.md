## Quiz        

1. The requirement specification
    - Turn what they want into the requirement.      

2. The verification and validation
    - The verification, what product the developer should build.
    - The validation, if the developer provide the right product to solve user's problem.

3. The requirement defect
    - It is important for software.
    - There is no formula.
    - It is expensive to fix.

4. The quality requirement characteristic
    - The correct requirement
        - The function.
        - The reference and source of requirement.
        - The user help correctness of requirement.
    - The feasible requirement
        - What can be done, exessive cost, and tradeoff.
        - The instantaneously is the problem.                
    - The necessary requirement
        - What user want.
        - Traceable to source.
        - The user not ask for.
    -  The prioritize requirement
        - We have time to fix the problem.
        - Assigned by the user.
        - Manager control. 
        - The priority level.
    - The unambiguous requirement
        - Do not use word, easy, simple, rapid, quick, and efficient.
    - The complete requirement
        - TBD
    - The consistent requirement
        - No conflict.
        - No disagreement.
        - No inconsistency.      
    - The reaceable requirement
        - Link to source.
        - Link to requirement.
        - Use different label for every requirement.
        - Express requirement in structured way.
    - The quality requirement
        - Keep sentence short.
        - Use active voice.
        - Grammar and spelling.
        - Consistent word.
        - Consistent level of detail.
        - No redundant.
    - The test deriven requirement
        - Revire the requirement for interation.
        - Get feedback.
        - Develop test case.
        - Get feedback.

5. The requirement management tool
    - The office tool.
    - The industry tool.

    
     




