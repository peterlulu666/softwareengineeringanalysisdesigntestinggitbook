## Lecture      

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/128qUTMoyOqK2qaaU4ULE7-UwRMFeVVwh/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1_n5RDvEGIuwq8IbkkmlLJjO-ws81f9NJ/preview"></iframe>

## Quiz      

1. Logic Coverage
    - We are still looking at **expressions** independent of source code but will use **source code** constructs as they might be specified in requirements
    - A logical **expression** is a **decision**, and the **decision** consists of **conditions**, conjoined by Boolean **operators** (and, or, ...). A **condition** contains **no** Boolean **operators**
    - A **decision** is a Boolean **expression** composed of conditions and zero or more Boolean **operators**. A decision **without** a Boolean **operator** is a **condition**
    - Logic Coverage
    - <img src="img/Logic Coverage.png" alt="Logic Coverage" width="500" height="600">

2. Decision coverage
    - For each Decision: 
        - Have **at least one** test where it evaluates to **true** 
        - Have **at least one test** where it evaluates to **false** 
    - Also known as: 
        - Predicate coverage 
        - Statement coverage (when we get to code) 
    - From a testing standpoint it is the weakest logic coverage
    - It does not test the individual Conditions only that the overall Decision is evaluated to either True or False

3. Condition Coverage
    - A **condition** is a leaf-level Boolean **expression** (it cannot be broken down into a simpler Boolean expression) 
    - For each Condition in a Decision: 
        - Evaluate to true 
        - Evaluate to false 
    - Also known as Clause (or condition) coverage
    - There are methods to gain additional compound conditions (like Condition/Decision Coverage)

4. Condition/Decision Coverage
    - When applied to logical statements the **requirement** is… 
    - Every **condition** in a decision has taken all possible outcomes **at least once**, and every **decision** has taken all possible outcomes **at least once** 
    - This is a requirement for DO-178 which is used world-wide for commercial aerospace applications. It is used for the second most stringent coverage criteria – applications that have some safety involvement 
    - This is also a criteria used for Security relevant applications

5. MCDC Coverage and Test Case Design
    - Modified Condition/Decision Coverage is the **highest** (and most expensive level of test coverage) of logical statements
    - Every **condition** in a decision has taken on all possible outcomes **at least once**, and **each condition has been shown to affect that decision’s outcome independently**. A condition is shown to affect a decision's outcome independently by varying **just that condition** while holding fixed all other possible conditions

6. MCDC Coverage and Basic Logic
    - I want to stimulate one input at a time and satisfy independence requirements 
        - TRUE,TRUE is the only test that returns a TRUE decision so I must chose this 
        - FALSE,TRUE is required as it is the only test that changes the value of only A which changes the decision to FALSE - establishing the independence of A – ANDs are sensitive to FALSE 
        - Similarly for B – since ANDs are sensitive to FALSE then I want to only set B to FALSE and A to TRUE, which will cause a FALSE decision 
    - We have shown above that a FALSE value for A alone causes the decision to be FALSE , similarly for B independently of A

7. White-box Testing Techniques
    - Basis Path Testing 
        - Flow Graph Notation 
        - Cyclomatic Complexity 
        Deriving Test Cases 
    - Condition Testing 
    - Data Flow Testing 
    - Loop Testing 
    - Symbolic Execution

8. Basis Path Testing Steps
    - Construct a flow graph
        - sequential
        - if-then-else
        - case
        - until
        - while
        - <img src="img/Construct a flow graph.png" alt="Construct a flow graph" width="500" height="600">
        - <img src="img/An Example Flow Graph.png" alt="An Example Flow Graph" width="500" height="600">
    - Compute cyclomatic complexity
        - Three ways to compute cyclomatic complexity: 
            - Number of closed regions + 1 
            - Number of atomic binary predicates + 1 
            - Number of Edges - Number of Nodes + 2
            - The cyclomatic complexity is 2+1=3
        - <img src="img/Compute cyclomatic complexity.png" alt="Compute cyclomatic complexity" width="500" height="600">
    - Determine basis paths
        - The total number of test cases needed to exercise all basis paths equals the cyclomatic complexity 
        - A basis path is 
            - A path from the begin node to the end node AND 
            - Traverses a cycle either zero times or exactly once 
        - The basis paths are: 
            - begin, 1, 2, 3, 4, 2, 5, end 
            - begin, 1, 2, 3, 4, 5, end 
            - begin, 1, 2, 5, end
    - Check to ensure that the number of basis paths equals to the cyclomatic complexity 
    - Derive test cases to exercise the basis paths according to the required coverage criteria

9. Data Flow Testing
    - Select test paths of a program according to the locations of definitions and uses of variables in the program, then test these define-use **chains** 
    - Steps 
        - Determine define-use **chains** for each variables 
        - Determine testing **criteria**, ex.  all-defines, all-uses, … 
        - Design test cases to satisfy the test **criteria**
    - The use of a variable can be either a c-use (computation use) or a p-use (predicate use) 
    - A def-use chain of a variable is a path from the definition to the use of the variable without any intervening redefinitions (i.e., definition-clear) 
        - for c-uses, the def-use chains are paths from the statement containing the definition to the statement containing the computation use 
        - for p-uses, the def-use chains are paths from the statement containing the definition to two successors of the statement containing the predicate use 
    - Test paths of a program are then selected based on the def- use **chains** of variables and test data adequacy **criteria** (e.g., all-use)
    - <iframe height="842" width="100%" src="img/Data Flow Testing.pdf"></iframe>

10. Loop Testing
    - <iframe height="842" width="100%" src="img/Loop Testing.pdf"></iframe>

11. A Generic Testing Process
    - <iframe height="842" width="100%" src="img/A Generic Testing Process.pdf"></iframe>

12. Use Case Based Testing
    - Agile Approach 
        - Step 1. Prioritize the use cases in the requirement-use case traceability matrix 
        - Step 2. Generate test scenarios from the expanded use cases 
        - Step 3. Identify test cases 
        - Step 4. Generate test data (values) 
        - Step 5. Generate test scripts 
    - Why do we prioritize use cases? 
        - We may not have needed resources or time 
        - Prioritizing ensures that high-priority use cases are tested 
    - The Requirement-Use Case Traceability Matrix (RUCTM) 
        - Rows are requirements and columns are use cases 
        - An entry is checked if the use case realizes the requirement – The bottom row sums up the priority of each use case

13. Generating Use Case Scenarios
    - A **scenario** is an instance of a use case 
    - A **scenario** is a concrete example showing how a user would use the system 
    - A use case has a primary **scenario** (the normal case) and a number of secondary **scenarios** (the rare or exceptional cases) 
    - This step should generate a sufficient set of **scenarios** for testing the use case 
        - Normal use cases 
        - Abnormal use cases

14. Register for Event
    - <iframe height="842" width="100%" src="img/Register for Event.pdf"></iframe>

15. Identifying Test Cases
    - Identify test cases from the scenarios using a test case matrix: 
        - The rows list the test cases identified 
        - The columns list the scenarios, the user input variables and system state variables, and the expected outcome for each scenario 
        - We’re going to apply techniques as before to identify test cases such as: 
            - Boundary Value Analysis 
            - Equivalence classes 
            - Decision tables

16. testing llevel
    - Unit testing 
        - Individual program units are tested in isolation. Unit testing is the **lowest** level of testing performed 
    - Integration testing 
        - Units are assembled to construct **larger** groups and tested 
    - System testing
        - Includes **wide** spectrum of testing such as functionality, and stress 
    - Acceptance testing 
        - Customer’s **expectations** from the system

17. Re-testing Level
    - During product evolution two kinds of testing occur: 
        - Testing of the new **changes** being made 
        - Testing to ensure that the new **changes** did not impact some existing functionality 
    - Regression testing is performed at software testing levels 
        - many times in parallel 
    - Regression testing is performed to ensure that the software function wasn't **impacted** because of: 
        - Unforeseen linkages/impacts between software components 
        - Subtle timing or tasking issues that cannot be verified other than test 
        - Configuration management problems
    - <img src="img/Re-testing Level.png" alt="Re-testing Level" width="500" height="600">

18. Monitoring and Measuring Test Execution
    - Each level of test has specific measures used to monitor and measure test executions and progress 
    - Metrics for monitoring test execution 
        - Test cases reviewed 
        - Test cases developed, debugged, and executing 
        - Test cases captured in the developmental CM system 
    - Metrics for monitoring defects 
        - Test cases passing/failing 
        - Test cases blocked (awaiting a software fix) 
        - Software fixes targeted for a specific release 
    - Test case effectiveness metrics 
        - Measure the “defect revealing ability” of the test suite 
        - Use the metric to improve the test design process 
    - Test-effort effectiveness metrics 
        - Number of defects found by the customers that were not found by the test engineers

19. Test Team Organization and Management
    - Unit and Integration testing is many times performed by the developers 
    - System level test and Acceptance testing is typically performed by **independent** test organizations 
        - “Independent” means not the software group 
    - Technology obsolescence in software development and test is very high 
        - New training must be continually provided to maintain an efficient work force 
    - To retain test engineers, management must recognize the importance of testing efforts at par with development efforts
    - A Notional Organization Structure of Test Groups
    - <img src="img/A Notional Organization Structure of Test Groups.png" alt="A Notional Organization Structure of Test Groups" width="500" height="600">

20. Test Documentation
    - Testing is a complicated task 
        - Mature organizations have a test process, test standard, and test plan 
        - These address all test activities: Unit, Integration, and System testing and include Regression test 
    - Test Process 
        - Test methodologies (e.g., specification based, code based, etc.) 
        - Test organization (how tests are organized, where kept, how changed, etc.) 
        - Who approves various test plans, documents, and artifacts 
        - Organizational roles and responsibilities – Test Tools and automation 
        - Training of personnel (new and existing) 
    - Test Standard 
        - Describes when testing is complete and the various activities used to support test completion

21. Test Plan
    - The purpose is to get ready and organized for test execution 
    - A test plan provides a: 
        - Framework 
            - A set of ideas, facts or circumstances within which the tests will be conducted 
        - Scope 
            - The domain or extent of the test activities 
        - Details of resource needed 
        - Effort required 
        - Schedule of activities 
        - Test objectives 
            - Identified from different sources

22. Test Procedures
    - Documented set of test cases needed to complete a test per the test plan 
    - Typically, 
        - Each test case is designed as a combination of modular test components called test steps 
        - Test steps are combined together to create more complex tests 
    - Test procedures may be **manual** procedures, **automated** procedures, or a **combination** of manual and automated procedures 
        - In accordance with the Test Plan 
    - Test procedures provide test steps, pass/failure criteria, and a record of the performance of tests

23. Test Results
    - These are the results of the test execution across all test activities: Unit, Integration, and System testing including regression tests 
        - May be a single report or provided in multiple reports 
    - Test results are typically tied to a specific product configuration 
        - Such as a specific test or delivery milestone 
    - Test results are used as evidence to demonstrate the software function(s) correctly works as specified by the requirements 
    - Test results may indicate or uncover one or more deficiencies as described earlier in the course

24. Unit- What is it
    - This is the age old question 
    - The industry has many different definitions for what constitutes a unit - the differing terminology is because the term unit has evolved over time as code went from 
        - Large files of assembly code 
        - Smaller files of high-order programming language 
        - More organized methods of code organization - such as structured coding 
        - Object-oriented software 
    - A unit can be a method, a class, or a file 
        - it is supposed to represent the smallest "unit" or piece of code
    - Rather than argue about what a unit is, we can understand that code has to be tested in this fashion (multiples of these can be grouped into a single test activity) 
        - Methods 
        - Within a single class 
        - Across Classes 
        - As a whole 
    - When we test from smallest to largest we have the most efficient test approach because the test being developed and conducted has the most narrow scope - at the unit level 
    - It is the most efficient because 
        - It has the **least** interference from outside forces (other changing or not yet developed units) 
        - It is the **easiest** to develop test cases for - the number of inputs and possible combinations is drastically reduced

25. Object Oriented Code Truths
    - About 85 percent of the object-oriented code base will have a **cyclomatic** complexity of 3 or less 
        - Much of the code is getters/setters 
        - The highest complexity code will be the controllers (the class causing things to happen) 
        - Typically, errors are related to the cyclomatic complexity so concentrating on testing the controllers is one method to reduce errors 
    - The object-oriented approach of developing software that has **high**- cohesion, **low**-coupling, information hiding, and encapsulation meaning that it is most suited to unit level testing 
    - It also means that much of the complexity from traditional structured approaches is now moved to the interface between classes 
        - Integration testing between classes is essential

26. Unit Testing
    - Involves testing a single isolated unit 
    - Note that unit testing allows us to isolate the errors to a single unit 
        - We know that if we find an error during unit testing it is in the unit we are testing 
    - Units in a program are not isolated, they interact with each other. Possible interactions: 
        - Calling procedures in other units 
        - Receiving procedure calls from other units 
        - Sharing variables 
    - For unit testing, we need to isolate the unit we want to test; we do this using: 
    - Drivers 
    - stubs 
    - mock objects

27. Advantages of Unit Testing
    - Advantages of Unit Testing: 
        - Faster Debugging 
            - Unit tests smaller amounts of code, easily isolating points of error and narrowing the scope for errors 
        - Faster Overall Development 
            - Less time debugging with unit tests 
            - Encourages aggressive refactoring resulting in better design and easier maintenance 
            - Adding enhancements can be performed with confidence that existing behavior of the system continues to operate in the same manner 
                - Modify current unit test in add tests for enhancements 
        - Better Overall Design 
            - As mentioned before, unit testing encouraging more refactoring 
            - Unit tests make developers focus more on the contracts of a class and those classes that might use it 
        - Excellent Regression Tool 
            - Major refactoring and restructuring of the code can be performed 
        - Reduce Future Cost 
            - Unit testing is an investment in the project. It takes time to ramp up, but in the long-term provides a useful basis for the project

28. Iterative Software Development
    - <iframe height="842" width="100%" src="img/Iterative Software Development.pdf"></iframe>

29. Unit Testing Framework
    - The environment of a unit is emulated and tested in isolation 
    - The caller unit is known as test driver 
        - A test driver is a program that invokes the Unit Under Test (UUT) 
        - The Test driver provides input data to the UUT and reports the test result 
    - The emulation of the units called by the UUT are called stubs 
        - It is a dummy program 
    - A mock object is a test program that substitutes for the actual object method 
        - The mock simulates the interface and behavior of the real method 
    - The test driver, the stubs, and mock objects are together called scaffolding 
    - The low-level design documents provide guidance for selection of input test data
    - A driver is the thing that calls the UUT 
    - Typically the driver is a stand-alone class/file that is not part of the delivered software 
        - It is stand-alone to make it easily separable from the deliverable software 
    - When the UUT calls another method outside its class it is typically stubbed out 
        - This means that the test case supplies the correct return value from that method (instead of actually calling it and getting the return) 
        - This helps to isolate one unit from another so that they can independently change 
        - As the units mature they may either be un-stubbed or stay stubbed

30. Why use a Testing Framework
    - Each class must be tested when it is developed 
    - Each class needs a regression test 
    - Regression tests need to have standard interfaces 
    - Thus, we can build the regression test when building the class and reuse it again later when the unit needs to be retested or modified and retested 
        - Provides for a better, more stable product for less work

31. Drivers, Stubs, and Mock Objects for Unit Testing
    - Driver 
        - A method (or main) that passes test case data to the component being tested, verifies the returned results, and summarizes pass/fail outcomes 
        - JUnit is such a driver, some drivers are developed by hand without tools 
    - Test double 
        - Substitution for production code during test - typically either a mock or stub (fakes and spies are also used) 
        - Both stubs and mocks are used to force the method being called to return the needed value to the UUT 
    - Stubs 
        - Hand-written classes that mimic the methods behavior, but do that with significant simplification 
    - Mocks 
        - Dynamic "wrapper" used to simulate a method call/return using a tool 
        - They use the method’s exact interface and are replaced by production code for later integration tests 
    - Drivers, stubs, and mock objects represent overhead test cost scaffolding) 
        - This in addition to production code
    - Stubs provide canned answers to calls made during the test, usually not responding at all to anything outside what's programmed in for the test 
        - Stubs may also record information about calls, such as an email gateway stub that remembers the messages it 'sent', or maybe only how many messages it 'sent’ 
    - Mocks are objects pre-programmed with expectations which form a specification of the calls they are expected to receive (more sophisticated state behavior interaction). 
    - Of these, only mocks objects simulate the behavior of the actual object

32. Advantage of Using a Test Automation Tool
    - Driver 
        - The driver is limited to a tool such as JUnit 
        - Other methods are even more automated providing sophisticated test case design and test reporting functions 
        - The driver code required is anywhere from somewhat less to considerable less 
    - Stubs 
        - Some tools provide automated methods to stub out method calls and returns so that this is totally invisible to the test case 
    - Mock Objects 
        - Some tools provide sophisticated means to develop mock objects and return appropriate test inputs and outputs 
    - Drivers, stubs, and mock objects both represent overhead 
        - The overhead is limited because the tool is generating this behind the scenes
    - Expense 
        - Some of these tools have a significant upfront and yearly cost 
    - Learning curve 
        - Many of these tools can take months to become proficient to use the basic features 
        - Some of the tools are quite feature rich 
    - Driver 
        - The driver code can inject a considerable amount of hidden instrumentation in the code 
        - This can make the code performance under test very sluggish - making performance assessments difficult 
    - The tradeoff between a test tool and use of driver code is one that needs to be made from a cost, schedule, and quality standpoint

33. Regression Test
    - ISTQB defines Regression Testing as: 
        - Testing of  a previously tested program following modification to ensure that defects have not been introduced or uncovered in unchanged areas of the software as a result  of  the  changes  made.  Regression testing  is typically   performed  when  the  software  or  its  environment  is changed 
    - Note that the industry and academic use of this term is pretty loose 
        - The common fault is including testing of new changes - this does not expose a regression error 
        - During product evolution, two kinds of testing occurs 
            - Testing of the new changes being made 
            - Testing to ensure that the change did not impact some existing function
    - Regression testing is performed at software testing levels - many times in parallel 
    - Regression testing is performed to ensure that the software function was not impacted because of: 
        - Unforeseen linkages/impacts between software components 
        - Subtle timing or tasking issues that cannot be verified other than test 
        - Configuration management problems 
    - Some approaches (Agile) perform daily unit testing 
        - There are pros and cons associated with this approach 
        - I prefer to test a unit when the code for that unit is changed only (this includes changes to the interface or inheritance structure)

34. Integration Testing
    - There are several methods of performing integration testing 
        - Testing across classes 
    - Horizontal Integration Testing 
        - “Big bang” integration 
        - Bottom-up integration 
        - Top-down integration 
        - Middle-out integration 
    - Vertical Integration Testing 
    - As mentioned previously we need to test: 
        - Methods 
            - Within a single class 
        - Across Classes 
            - As a whole 
    - This is what integration seeks to achieve

35. Testing Web Applications
    - Web applications exploit 
        - Navigation and interaction facilities of web pages 
        - Requesting information from the user 
        - Providing information to the user 
    - Navigation and interaction patterns are important aspects of Web applications testing

36. Web Page
    - A Web page contains 
        - the information to be displayed to the user 
        - the navigation links to other pages 
        - organization and interaction facilities (e.g., frames and forms) 
    - Navigation from page to page is modeled by  links 
    - Contents of static pages are fixed 
    - Contents of dynamic pages are generated according to the input provided by the user (represented by the use attribute)

37. Relationships
    - A page can contain any number of forms 
    - A page may be split into a number of frames 
    - A page may include other pages 
    - Forms have input variables modeled by the input attribute of form 
    - Forms are submitted to dynamic pages

38. Frames
    - A frame is a rectangular area in the page where navigation can take place independently 
    - Frames on a page can interact with each other 
    - A link in a page of a frame can force the loading of a page into another frame

39. A Meta-model for a Web Application
    - <img src="img/A Meta-model for a Web Application.png" alt="A Meta-model for a Web Application" width="500" height="600">

40. Usefulness of a Meta Model
    - Defining test criteria 
    - Guiding the test process 
    - Detecting 
        - Non-compliance with user requirements 
        - abnormal situations (e.g., ghost pages, unreachable pages) 
    - Techniques used to accomplish above 
        - static analysis --- no code execution is required 
        - dynamic validation --- exercise the code/system and compare the outcomes with expected results

41. Static Analysis
    - Unreachable pages are pages that are available at the server site but cannot be reached by any path from an initial page 
    - Unreachable pages can be detected by using a graph traversal algorithm 
    - Ghost pages are pages that are referenced by a link but do not exist. Check should include external pages referenced by a page of the web application 
    - Ghost pages can be checked by visiting the URL
    - In web applications, pages should be loaded into appropriate frames 
    - The set of reachable frames for a given page 
        - frames that have the page as the initial page 
        - frames that is loaded with the page due to a link 
    - Reachable frames show the assignment of pages to frames 
    - Can be used to detect inappropriate assignment of pages to frames
    - Shortest path 
        - the minimal number of pages that must be visited before reaching a target page 
        - this information is an indication of potential problems for searching a page if the shortest path is long 
    - Comparing the above metrics for different versions of a web site may be used to access the quality improvement (or not)

42. Static Analysis: Data Flow Testing
    - Select test paths of a program according to the locations of definitions and uses of variables in the program, then test these define-use chains 
    - Steps 
        - Determine define-use chains for each variables 
        - Determine testing criteria 
        - Design test cases to satisfy the test criteria

43. Data Flow Testing for Web Applications
    - The input attributes of a Form define the input variables 
    - The definitions propagate along the edges of the site graph 
    - If the definition reaches a dynamic page which uses one of the variables, a define-use dependency is identified 
    - Traditional data flow analysis techniques are then used to analyze the define-use path

44. Test Cases/Dynamic Analysis
    - A test case is defined as: 
        - A sequence of URLs to be visited, along with 
        - The input variable values that must be supplied 
    - Execution of a test case is: 
        - Requesting the web server for the URLs along with the input values, and 
        - Storing the output pages 
    - Unlike traditional program testing, URLs can be directly visited without having to satisfy the path conditions

45. Coverage Criteria
    - **Page coverage**: every **page** in the site is visited at least once in some test case 
    - **Link coverage**: every **hyperlink** from every page in the site is traversed at least once 
    - **Define-use coverage**: **at least one** navigation path from every definition of a variable to every use of it, forming a data dependence, is exercised 
    - **All-uses coverage**: **all** navigation paths from every definition of a variable to every use of it, forming a data dependence, is exercised 
    - **All-paths coverage**: every path in the site is traversed in some test case **at least once**

46. Removing Static Pages
    - Static pages without a form displays only static information 
    - Such static pages can be removed from the site graph during dynamic validation 
    - The predecessors of a static page is directly linked to all of its successors 
    - In the resulting graph, an entry node and an exit node are added

47. Entry Node and Exit Node
    - An entry node is added and is connected to every node that does not have a predecessor 
    - An exit node is added and is reachable from all dynamic pages with non-empty user input variables, because: 
        - There is no intrinsic notion of termination of a computation in web browsing 
        - Whenever information is displayed to the user, the computation is completed

48. When do we Stop Testing?
    - Testing costs money 
        - “If the design and code job is done correctly, we don’t need to test.”       -- anonymous 
        - This does not ever work in practice! 
            - People make mistakes – At every step and substep of the development process 
            - Tools can insert faults into the work products 
            - Test tools (for verification and/or validation) can have faults in them too 
        - Test activities must be planned, reviewed, executed, and reviewed 
            - Maintaining a Test organization is like maintaining any other organization in engineering 
                - Requires budget, investment, costing, accountability 
        - The more we test, the more it costs 
            - Alternatively, the more we test, the better the product becomes 
    - So, when do we stop testing?
    - <iframe height="842" width="100%" src="img/When do we Stop Testing.pdf"></iframe>

49. Software Testing
    - Software testing is a dynamic validation technique 
    - Software test techniques include 
        - White Box (basis path, condition, Data Flow, Loop, Symbolic execution) 
        - Black Box 
        - Use-case based 
        - State dependent 
    - Software testing can detect errors in the software but cannot prove that the software is free of errors 
    - Test coverage provides assurance 
        - Logic Coverage 
        - Decision Coverage 
        - Condition Coverage 
        - Modified Condition/Decision Coverage 
    - Software testing increases the development team’s confidence in the software product























































































