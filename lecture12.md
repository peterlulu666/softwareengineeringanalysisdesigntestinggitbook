## Quiz      

1. Develop Sequence Diagram
    - Object interaction modeling helps the  development team 
        - Understand the existing business processes 
        - Design object interaction behaviors to improve the business 
    - Actor-system interaction modeling deals with foreground processing of a use case 
    - Object interaction modeling deals with background processing of a use case

2. Actor-System Interaction & Object Interaction
    - Actor-system interaction
        - Foreground processing of use case.
        - Acquiring actor input and actor action.
        - Displaying system responses.
    - Object interaction
        - Background processing of use case by objects.
        - Designing high-level algorithms to process actor requests.
        - Producing system responses.     

3. Two perspective
    - For sequence diagrams there are two perspectives to consider
        - 1. The **analysis** perspective 
            - how do the objects interact with each other to accomplish the business tasks in the existing, possibly manual business processes? 
        - 2. The **design** perspective 
            - how should the objects interact in the proposed system to improve the business process?
    - OIM is important from an **analysis** perspective because, the development team
        - May not be familiar with the existing business process 
        - May need to collect information and construct models to help understand them
        - May need object interaction models to identify problems or weaknesses in the existing business process
    - OIM is important from a **design** perspective because, the existing business processes may have been designed years ago
        - May have expanded considerably since then 
        - May have changed dramatically (technological improvements) 
        - Existing processes may need to be redesigned to better address technology or business needs

4. Difference Between Analysis and Design
    - Analysis 
        - Application problem-oriented 
        - Models application domain 
        - Describes what the world is 
        - Project-oriented decisions 
        - Should allow design alternatives
    - Design
        - Software solution-oriented 
        - Models the software system 
        - Prescribes a software solution 
        - System-oriented decisions 
        - Usually reduces implementation choices

5. How Does OIM Address Analysis and Design?
    - Modeling and analysis 
        - Helps understand how objects **interact** in the existing system or existing business processes 
        - Helps understand how objects **interact** in the proposed system or proposed business processes 
        - Helps identify existing problems and **limitations** of the system or existing business processes 
        - Helps identify new problems and **limitations** of the system or new business processes
    - Object interaction design 
        - Design and specify how objects interact for the proposed software system to **carry out the use cases** 
        - Designs object interaction behavior- the design of **sequences** of messages between the objects or how they collaborate 
        - Designs object interfacing – the design of the **signatures** and **return types** of the functions of objects 
        - Can use design patterns – application of **design patterns** to produce well designed interaction

6. Object Interaction Modeling
    - OIM specifies how objects interact with each other to carry out the background processing of a business process 
    - Object interaction modeling is aided by the specification of: 
        - **scenarios** 
        - **scenario tables** 
    - A **scenario** is an informal, step-by-step description of object interaction 
    - A **scenario table** organizes the object interaction into a five-column table 
        - The table facilitates translation of the object interaction into a sequence diagram

7. Object Interaction Modeling Steps
    - Collecting  info about business processes
        - expanded use cases for current iteration
    - Describing scenarios
    - Constructing scenario tables
    - Constructing sequence diagrams
        - domain model, design class diagram  from last  iteration
    - Review and inspection
        - design sequence diagrams to deriving a design class diagram
    - <img src="img/Object Interaction Modeling Steps.jpg" alt="Object Interaction Modeling Steps" width="500" height="600">

8. What is a Sequence Diagram?
    - <img src="img/What is a Sequence Diagram.jpg" alt="What is a Sequence Diagram" width="500" height="600"> 

9. Sequence Diagram Notions and Notations
    - <img src="img/Sequence Diagram Notions and Notations.jpg" alt="Sequence Diagram Notions and Notations" width="500" height="600">        
    - **Synchronous** messages – caller must wait until the message is done before issuing another call
    - **Asynchronous** messages – caller may continue processing and does not need to wait for a response
    - <img src="img/Sequence Diagram.jpg" alt="Sequence Diagram" width="500" height="600">

10. Operators in a Combined Fragment
    - A combined fragment is an interaction fragment which defines a combination (expression) of interaction fragments. A combined fragment is defined by an interaction operator and corresponding interaction operands. Through the use of combined fragments the user is able to describe a number of traces in a compact and concise manner.      
    - <img src="img/Operators in a Combined Fragment.jpg" alt="Operators in a Combined Fragment" width="500" height="600">

11. Reference Operator"in a Combined Fragment
    - <img src="img/Reference Operator in a Combined Fragment.jpg" alt="Reference Operator in a Combined Fragment" width="500" height="600">

12. Representing Various Object Instances
    - Notation
        - :Car
            - An unnamed instance of the Car class. The name is not important or is not used elsewhere in the sequence diagram.        
        - car:
            - A named instance of an unnamed class. A class without a type, the type is not important, unknown, or ‘to be determined’ at run time.      
        - car: Car
            - A named instance of the Car class. A named instance with a type; this is commonly used.      
        - :Car
            - A collection of instances of the Car class  or a collection of objects of the Car class.      
        - <<jsp>> LoginPage:
            - A stereotyped object. A stereotyped class might have some not directly supported in UML.      
        - <img src="img/Representing Various Object Instances.jpg" alt="Representing Various Object Instances" width="500" height="600">
        
13. Using the Notations Correctly
    - <img src="img/Using the Notations Correctly.jpg" alt="Using the Notations Correctly" width="500" height="600">

14. Sequence Diagrams Illustrated
    - Simplified Login use case 
        - 1. User submits UID and password to LoginPage 
        - 2. LoginPage verifies with LoginController using UID and password 
        - 3. LoginController gets user (object) from the database manager (DBMgr) using UID 
        - 4. DBMgr returns user (object) to LoginController 
        - 5. LoginController verifies with user (object) using password 
        - 6. User (object) returns result to LoginController 
        - 7. LoginController returns result to LoginPage 
        - 8. If result is true, LoginPage redirects to WelcomePage 
        - 9. If result is false, LoginPage shows an error message to user 
        - 10. User is shown the WelcomePage (or the error message)

15. Sequence Diagram of a Login Scenario
    - <img src="img/Sequence Diagram of a Login Scenario.jpg" alt="Sequence Diagram of a Login Scenario" width="500" height="600">

16. Sequence for a Checkout Document'use Case
    - <img src="img/Sequence for a Checkout Document'use Case.jpg" alt="Sequence for a Checkout Document'use Case" width="500" height="600">

17. Sequence Diagram for a Checkout Document
    - <img src="img/Sequence Diagram for a Checkout Document.jpg" alt="Sequence Diagram for a Checkout Document" width="500" height="600">

18. Sequence Diagram for Analysis and Design
    - The previous **Login** scenario was depicted as an Analysis Sequence diagram 
        - The messages were more like English text 
        - The exact formal interface has not yet been specified 
    - The previous **Checkout Document** scenario was depicted as a Design Sequence diagram 
        - The messages were much more formal looking like a programming language 
    - The former allows for learning and exploring ideas 
    - The latter is useful for . . . 
        - Applying design patterns 
        - Reusing existing components 
        - Deriving the design class diagram and information 
        - Generating skeletal code

19. Develop Sequence Diagram        
    - Classes from Domain model
    - Expanded Use Case (EUC) specifications
    - Sequence Diagram(s) of highest risk features for that iteration
    - <img src="img/Develop Sequence Diagram.jpg" alt="Develop Sequence Diagram" width="500" height="600">

20. How to Identify a Nontrivial Step?
    - A trivial step is . . .
        - If the step does **not** require **background** processing 
        - If the system response simply displays a **menu** or **input** dialog 
        - If the step displays the **same** system response for all actors      
            - For example, in Step 2 from the previous expanded use case the Checkout GUI is the same for all actors
    - A Nontrivial step is . . .
        - The system response requires **background** processing 
        - The system response is **different** for different actors (not just a standard GUI) 
        - Key question: does the system response require other objects to interact and collaborate with each other to fulfill the request? 
        - Checkout message of step 4 has the following considerations: 
            - 1. Does the document **exist**? 
            - 2. If the document exists, is it **available**? 
            - 3. Has to **create** a loan record 
            - 4. **Error** messages as appropriate above

21. Example
    - <img src="img/How to Identify a Nontrivial Step Example.jpg" alt="How to Identify a Nontrivial Step Example" width="500" height="600">

22. Modeling the Non-trivial Step
    - If we look at step 4 in the expanded use case – the step is . . .
        - “The checkout GUI displays a checkout message showing the details.”
        - This task can be broken down into a number of sub-tasks 
        - Checkout documents (use objects in task steps) • Get the document objects from the database 
            - Create loan objects 
            - Set document objects to unavailable 
            - Save loan objects to database 
            - Save document objects to database 
            - Return a checkout message

23. Guidelines for Scenario Construction
    - Specify the ‘normal scenario’ first (i.e., assume everything will go as expected; no errors)
    - If needed,  augment the normal scenario with alternative flows
    - Scenario writing steps
        - 1. Identify what must be done to fulfill the **non-trivial actor request** 
        - 2. Determine what **order** is needed to carry out these tasks 
        - 3. For each of these tasks, determine the **object that is acted upon** 
        - 4. For each of these tasks, determine the **object to issue the request** to perform them

24. Checkout Document Scenario Description
    - <img src="img/Checkout Document Scenario Description.jpg" alt="Checkout Document Scenario Description" width="500" height="600">

25. Constructing the Scenario Table
    - Each sentence of the scenario is a declarative sentence consisting of . . . 
        - 1. A subject 
        - 2. An action of the subject 
        - 3. An object that is acted upon 
        - 4. Possibly other objects required by the action
    - The sentences are arranged in a scenario table to . . .
        - Facilitate scenario description and 
        - Facilitate translation into a sequence diagram 
        - Note: for most of the rows the subject is either ‘the subject’ or ‘the object acted upon’ from the **previous row**
    - The approach is to . . .
        - 1. Highlight (or identify) the subject, the subject action, data or objects required by the subject action, and the object acted upon 
        - 2. Enter these items into the scenario table row by row
    - <img src="img/Constructing the Scenario Table.jpg" alt="Constructing the Scenario Table" width="500" height="600">

26. Writing Scenarios
    - In the previous example, the scenario description (i.e., the detailed steps) were written out for us before we started 
    - What if we must invent the steps?

27. Assigning Tasks to Objects
    - To determine which objects perform the task: 
        - 1. Look up the objects in the last two columns 
        - 2. Correlate to classes in the domain model – don’t introduce new classes 
        - 3. Look at related business documents
    - To determine which object should issue the request to perform each of the tasks: 
        - 1. If the task is a sub-task of a **previous row**, then the requesting object is on the **previous row** 
        - 2. If the task is not a sub-task of a **previous row**, then in most cases the requesting object is the subject of the **previous row** 
        - 3. For the first row, the requesting object is the GUI that receives the request and usually named after the use case        
    - <img src="img/Assigning Tasks to Objects.jpg" alt="Assigning Tasks to Objects" width="500" height="600">
    - <img src="img/Assigning Tasks to Objects objects perform.jpg" alt="Assigning Tasks to Objects objects perform" width="500" height="600">      
    - <img src="img/Assigning Tasks to Objects request.jpg" alt="Assigning Tasks to Objects request" width="500" height="600">      
    - <img src="img/Assigning Tasks to Objects all.jpg" alt="Assigning Tasks to Objects all" width="500" height="600">

28. Assigning Requestors
    - The first task is to ‘checkout document(s)’
    - Which object should issue the request to perform this task? 
        - According to rule 3 from the previous page, the GUI object should be named after the use case – ‘checkout GUI’
        - This also follows functionally because it receives the actor request to checkout the document(s) 
    - Next, which object should issue the request to ‘get documents?’ 
        - Since ‘get documents’ is a subtask of checkout documents, ‘checkout controller’ should issue the request because it is the object from the **previous row** 
        - The ‘checkout controller’ also issues requests for the remaining subtasks for the same reason 
    - Finally, which object should display the checkout message? 
        - Since it is not a subtask of the **previous tasks**, this object should be the subject of one of the **previous rows** – which is ‘checkout GUI’
    - The results from these design decisions are shown on the next slide 
    - The are two other activities that need to be performed to complete the scenario 
        - 1. For each task that returns a result, insert a row to return the results from ‘the object acted upon’ to ‘the requesting object’. There are two such tasks – get documents and checkout documents 
            - a. The get documents subtask should return the documents that are requested (as the name implies). So a row indicating that the DBMgr returns the document to the checkout controller is added after the get documents row. 
            - b. The checkout documents subtask should return a checkout message because the checkout GUI displays this message to the patron. But, this activity is already there, so no row is added.
        - 2. Conditional and loop statements are inserted as appropriate and statement numbers are entered in the first column 
    - The results of these two steps are shown on the next slide               

29. Converting Scenario Tables to Diagrams
    - Converting the scenario tables to diagrams involves the following three steps
        - Converting scenario tables to sequence diagrams. In this step, an informal sequence table is derived from each scenario table
        - Deciding on instance names and types. In this step the names and types of the object instances that send and receive messages are defined
        - Determine the function names, parameters, and return types        
    - <img src="img/Converting Scenario Tables to Diagrams.jpg" alt="Converting Scenario Tables to Diagrams" width="500" height="600">

30. Case 1
    - For each line of the scenario table
    - Case 1: subject is an Actor
    - <img src="img/Case 1.jpg" alt="Case 1" width="500" height="600">

31. Case 2
    - Case 2: subject is an object and the object acted upon is an actor
    - <img src="img/Case 2.jpg" alt="Case 2" width="500" height="600">

32. Case 3
    - Case 3: both subject and the object acted upon are objects
    - <img src="img/Case 3.jpg" alt="Case 3" width="500" height="600">      

33. Case 4
    - Case 4: both subject and object acted upon are the same (a special case of case 3)
    - <img src="img/Case 4.jpg" alt="Case 4" width="500" height="600">              

34. Deciding on Instance Names and Instance Types
    - Recall that when a scenario table is converted into a sequence diagram, the instance names and instance types are not specified
    - Deciding on instance names
        - 1. If an instance is used as a parameter or return value in the sequence diagram, then give it a name so it can be used to refer to that instance      
        - 2. If the instance has a unique connotation outside the sequence diagram (Java Server Page) then give the instance a name and make it a stereotype instance e.g., <<html>>WelcomePage:      
    - Deciding on instance types
        - 1. If the needed class is not found on the existing sequence diagram or domain diagram, the create a new class 
        - 2. When deciding on the class of the elements of a collection, two different cases are considered 
            - a) If the elements of the collection are instances of only one class, then that class is the class for its elements 
            - b) If the elements of the collection are instances of subclasses of a superclass, then the superclass is the class for its elements        

35. Deciding on Object Interfacing
    - Deciding on parameter types and return types is an application dependent issue as well as a software design consideration 
    - Some rules for formally specifying messages are as follows 
        - An action + other data/objects message (from the scenario table) is translated with a return value (if any) to: 
            - return_value:= action(other data/objects) 
            - Example: for a ‘verify uid,password’ message with a result of ‘verification_result’ is formally specified as: verification_result := verify(uid,password)        

36. Formal Parameter Types on Sequence Diagrams
    - <img src="img/Formal Parameter Types on Sequence Diagrams.jpg" alt="Formal Parameter Types on Sequence Diagrams" width="500" height="600">        

37. Object Interaction Coupling
    - <img src="img/Object Interaction Coupling.jpg" alt="Object Interaction Coupling" width="500" height="600">        











