## Quiz      

1. Interaction Modeling Review checklist
    - Ensure that the scenarios and sequence diagrams correctly and adequately describe the improved business process
    - Ensure that all messages of a design sequence diagram are formally specified
    - Ensure that there is no use of dashed arrow lines to return a result to an object
    - Ensure that all required parameters and return values are present and the parameters are defined before being used in a function call
    - Ensure that function names properly communicate the functionality
    - Ensure that each sequence diagram satisfiers all of the following conditions
        - The sequence diagram is syntactically correct
        - The sequence diagram is semantically correct

2. Analysis sequence diagram
    - <iframe height="842" width="100%" src="img/analysis sequence diagram.pdf"></iframe>                        

3. Applying Agile Principles
    - Work closely with the customer and users to understand their business processes, especially how the business processes the transactions
    - The team members write the scenarios and construct the scenario tables **together**. Do not do these **individually**
    - No need to construct the scenario, scenario table, or sequence diagram if the team members understand the scenario very well
    - Converting the scenario table or scenario to a sequence diagram should be done **off-line** and be a **one-person job**
    - Keep it simple and stupid. Omit programming detail in the scenario and the sequence diagram
    - Constructing prototypes to remove doubt

4. Desion Patterns
    - Patterns have been used in other fields, disciplines
        - Weather, stock movement predictions, models, etc.
    - Design patterns are abstractions of proven design solutions to commonly encountered software design problems or issues
    - The controller, expert, and creator patterns are applicable to almost all OO systems

5. What Are Design Patterns?
    - Design patterns are proven design solutions to commonly encountered design problems
        - Each pattern solves a class of design problems
    - Design patterns codify software design principles and idiomatic solutions
    - Design patterns improve communication among software developers
    - Design patterns empower less experienced developers to produce high-quality designs
    - Patterns can be combined to solve a large complex design problem

6. Describing Patterns
    - The pattern name conveys the design problem as well as the design solution
    - Example: Singleton Pattern
        - How does one design a class that has only one globally accessible instance?
        - The singleton pattern provides a solution
    - Pattern descriptions also specify
        - Benefits of applying the pattern
        - Liabilities associate with the pattern, and
        - Possible trade-offs
    - Pattern specifications are templated        

7. Example: The Singleton Pattern
    - Pattern name: Singleton
    - Design Problem: How do we ensure that a class has only one globally accessible instance?
    - Design Solution: Make a constructor of the class private and define a public method to control creation of instances
    - Example uses
        - System configuration class
        - System log file        

8. Specification of Singleton Pattern
    - <iframe height="842" width="100%" src="img/Specification of Singleton Pattern.pdf"></iframe>

9. When to Use a Singleton
    - Singleton's are over used and should be avoided almost at all costs. However, there are a few limited cases where is makes sense to use a Singleton
        - **Hardware interface access**: Singleton can be used in case external hardware resource usage **limitation** are required        
            - Example: Hardware printers where the print spooler is a singleton (to avoid multiple concurrent accesses and creating deadlock)      
        - **Logger** : If there are multiple client applications using the **logging** utility class they might create multiple instances and cause issues during concurrent access to the same logger file        
        - **Configuration File**: Here, create a **single** instance of the **configuration** file which can be accessed by **multiple calls** concurrently        
        - **Cache**: Cache can be used as a singleton object as it can have a global point of reference and for all future calls to the cache object the client application will use the single object        

10. When to Avoid Singletons
    - First and foremost, if you have Singleton's in your design you should rethink your design - there is probably a better way to design the class structures      
    - Some use these as a way to avoid **global data**. In situations where you feel pressed to use **global data** you should consider these
        - Why do I need to use global data/singletons at all? Isn't there a better design approach that will eliminate this perceived need
        - If the **global data** must be used then consider - which is worse the use of global data or a singleton. This may be more of a **personal preference**. It takes more **effort** to create a singleton than global data and they limit polymorphism and other features significantly
        - Can I pass references to objects in my classes and methods, which will reduce the **coupling** issues
        - Singletons have **persistent** data that exists the entire lifetime of the program. Is this what I want to do

11. More About Design Patterns
    - Patterns are recurring designs
    - Patterns are not new designs
    - Most patterns aim at improving the maintainability of the software system
        - Easy to understand
        - Easy to change
    - Some patterns also improve efficiency or performance

12. Commonly Used Design Patterns
    - The General Responsibility Assignment Software Patterns (GRASP)
    - The Gang of Four (GoF) Patterns
        - Name GoF is due to the four authors of the book

13. GRASP Patterns
    - The GRASP patterns help us decide which responsibility should be assigned to which object
        - Singleton
        - Creator
        - Controller
        - Expert

14. Gang of Four Patters
    - GoF design patterns are situation-specific, each solving a specific class of design problem
        - **Creational patterns** deal with creation of complex or special purpose objects
        - **Structural patterns** provide solutions for composing or constructing large, complex structures that exhibit desired properties
        - **Behavioral patterns** are concerned with
            - **algorithmic** aspect of a design
            - assignment of **responsibilities** to objects
            - **communication** between objects

15. The Gof Patterns
    - Creational
    - Structural
    - Behavioral

16. Applying GRASP through a Case Study
    - Examine a commonly seen design
    - Discuss its pros and cons
    - Apply a GRASP pattern to improve
    - Discuss how the pattern improves the design
    - During this process, software design principles are explained

17. Problems with This Design
    - Tight coupling between the presentation and the business objects
    - The presentation has been assigned too many responsibilities
    - The presentation has to handle actor requests (also called system events)
    - Implications
        - Not designing “stupid objects”
        - Changes to one object may require changes to the other
        - Supporting multiple presentations is difficult and costly

18. A Better Solution
    - Reduce or eliminate the coupling between presentation and business objects
        - Low Coupling design principle
    - Remove irrelevant responsibilities from the presentation
        - Separation of concerns principle
        - Achieves high cohesion and
        - Designing “stupid objects”
    - Have another object (class) to handle actor requests (system events)

19. Who Should Handle an Actor Request?
    - Assign the responsibility for handling an actor request to a controller

20. The Controller Pattern
    - Actor requests should be handled in the business object layer
    - Assign the responsibility for handling an actor request to a controller
    - The controller may delegate the request to business objects
    - The controller may collaborate with business objects to jointly handle the actor request
    - Use of the Controller Pattern supports the following design principles
        - **Design for change**. It insulates the changes to business objects/logic from the presentation (GUI)
        - **Separation of concerns**. The GUI only deals with the presentation aspects while the controller is responsible for processing
        - **High Cohesion**.  The segregation into a presentation and processing class helps to increase cohesion
        - **Designing "stupid objects"**. Each object has a specific focus and knows only how to do that task

21. Types of Controller
    - Use case controller
        - Handles all actor requests relating to a use case
            - A checkout controller handles all actor requests to checkout a document
            - This is the most common - especially for our class
    - Facade controller
        - It represents the overall system (Library System, Banking System)
        - It represents the organization (Library, Bank)
        - It is used when there are only a few use cases in the entire system
    - Connections with other objects in sequence diagrams
        - The controller is almost always the object(s) that have the most connections with the rest of the system
        - It represents much of the code and cyclomatic complexity of the software
            - it will have the most methods
    
22. When to Apply the Controller Pattern
    - Apply when writing the use case scenario
        - The best time to apply this pattern is when the use case is being written - no rework is required to apply this pattern afterward
    - Apply by modifying an existing scenario
        - In this case the scenario can be modified to use the controller pattern
    - Apply when constructing the scenario table
        - Although not as attractive as applying it earlier, it still offers the advantages of good design principles described earlier
    - Apply when constructing the design sequence diagram
    - Apply by modifying a design sequence diagram

23. Applying Use Case Controller
    - Applying role controller or facade controller
        - Applying role controller or facade controller
            - When there are only **a few system events** and system will **not expand** in the future
                - Example: interlibrary loan
        - Applying the Use Case controller
            - Each use case has its **own use case controller**
                - **Checkout** Controller for **Checkout** Use Case
                - **Return** Controller for **Return** Use Case
            - There is only one controller for each use case
            - There is a one-to-one correspondence
                - **Checkout** Use Case -- **Checkout** GUI, **Checkout** Controller
                - **Login** Use Case -- **Login** GUI, **Login** Controller
            - The Use Case Controller maintains the state of the use case and identifies out-of-sequence system events

24. Liabilities of The Controller Pattern
    - More classes to design, implement, test and integrate
    - Need to coordinate with the developers who design and implement the user interface, controllers, and business objects
        - This is not a problem when the methodology is followed
    - If not designed properly, it may result in bloated controllers

25. Controller Pattern Guicelines
    - Separate the design into GUI, Controller, and Mgr objects
    - Never put the UI in the Controller object
    - Never put the business interface (e.g., DB) in the Controller object
    - Processing, sequencing ,logic, procedures, and algorithms should go in the Controller class
    - Each Use Case should have its own Controller class
    - The Mgr class should perform all transactions with the business interface
        - Searching attributes, updating attributes, querying attributes, etc.
        - A network interface should have its own Mgr object

26. Bloated Controller
    - A bloated controller is one that is assigned too many **unrelated** responsibilities
    - Symptoms
        - There is only **one** controller to **handle** **many** actor requests
            - This is often seen with a role controller or a facade controller
        - The **controller** does **everything** to handle the actor requests rather than delegating the responsibilities to other business objects
        - The **controller** has **many attributes** to store system or domain information

27. Cures to Boated Controllers
    - Symptoms
        - Only one controller to process many system events
    - Cures
        - Replace the controller with **use case controllers** to handle use case related events
    - Symptoms
        - The controller does all things rather than delegating them to business objects
    - Cures
        - Change the controller to delegate responsibilities to **appropriate business objects**
    - Symptoms
        - The controller has many attributes to maintain system or domain information
    - Cures
        - Apply separation of concerns: move the attributes to business **objects** or other objects








