## Quiz         

1. The agile method is for small and medium size project.

2. The plan driven approach is for large and complex peoject.      

3. The planning phase
    - The acquiring
    - The deriving
    - The allocating
    - The producing

4. The iterative phase, iteration
    - The use case
    - The domain domel
    - The expanded use case
    - The behavor diagram
    - The design class diagram

5. The system engineering
    - The multidisciplinary
    - The organization
        - The requirement and constraint.
        - It allocates the requirement. 
        - The software engineering is part of system engineering.
    - It brings all the disciplines to design and build.      

6. The system
    - It consist of interrelated component.
    - It consist of component that interact with each other.
    - Big, small, simple, complex, natural, man made, physical, conceptual.
    - The universe, ant, logic, system.
        - The system consist of interrelated and interacting system.
        - The subsystem, the hierarchy of system.
        - The system exists in the environment and interacts with the environment.
        - The system is evolving.      

7. The system engineering is characterized by interdisciplinary and multidisciplinary        
    - Electrical and electronic engineering.      
    - Mechanical engineering.      
    - Civil engineering.      
    - Software engineering.        
    - Computer science.        
    - Business administration and economics.        

8. The system engineering is characterized by a system engineering process that covers the entire life cycle of the system    
    - It is beginning with the initial system concept to the maintenance and retirement of the system.        

9. The system engineering is characterized by a top down divide and conquer approach
    - This approach decomposes the total system into a hierarchy of subsystems and components, and develops each of them separately. There is a lot component. They will understand the component. Where they are. Why they are fit the way they are.                

10. The system engineering process        
    - The definition, design, software and hardware development, testing, deployment, and maintenance.      

11. The system requirement definition
    - Identify business needs.
        - collect info about business goal and current situation using presentation, study, survey, and interview.
    - Define system requirement.
        - Ensure that the team have the capabilities to get the project done with the budge and schedule constraint.
        - The project plan.      

12. The system architectural design
    - Decompose the system
        - Decompose the system into subsystem.  
        - Decompose the system according to function.
        - Decompose the system according to engineering disciplines.        
        - Decompose the system according to the existing architecture. It will reduce cost and risk.          
    - Allocate system requirement
        - Assign to subsystem.      
        - It considers function, cost, performance, quality, modularity, cohensiveness, and easy of change.
    - Visualize system architecture
        - The diagram that shows component, connection, and constraint.        
        - There is block diagram, UML, SysML, and Data Flow diagram.         

13. The requirement traceability
    - Visualize.
    - Show allocation appropriate.
    - Show requirement assigned.

13. The guidelines for system decomposition.      

14. The strategies for system decomposition.        





























