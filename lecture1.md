## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1Udi3KukolChiarS2mnSZsa7goF8DKp4g/preview"></iframe>

## Lecture Video

<iframe src="https://drive.google.com/file/d/1gFHhprNEyKweBtXbHbKqEaaLTKubVSJW/preview" 
width="640" 
height="480"
allowfullscreen="allowfullscreen"></iframe>

## Quiz

1. The programming and the software engineering
    - The programming, it is the strengthof the individual programmer
    - The software engineering, we should learn from the past mistakes. We should know what works. If we find the better one, we will make changes. We should improve software productivity and quality as well as reduce software cost and time.       

2. What makes the software engineering?
    - Writing programs that consist of millions of lines of code is a common practice in the real world. They have to overcome many
other challenges as well. To overcome these challenges, a disciplined approach to software engineering is required.            
    - They work in the team with the other software engineers. They take a long time to write hundreds and thousands lines of code. They should cope with conceptualization, communication, and coordination problem.      

3. The UML
    - The natural language is informal and it will lead to misunderstanding. So the software engineers use UML to communicate their idea.     
    - It is used in the industry.       

4. The production line
    - In every section, they have standard and the right tool.
    - Idea, requirement, design, code, test, and product.

5. The work defect
    - Understand how many error
    - budget, schedule, and tolerate















