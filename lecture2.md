## Quiz      

1. The software and the engineering
    - The software is design, reuse, practiced, malleable, a lot of problem, obsolete, and bug

2. The track
    - Development. Requirement, design, code and test.
    - Quality. Ensure the standard. Verification, validation user participation, and testing. 
    - Management

3. The SQA
    - SQA ensures that the software system under development will satisfy the software
requirements and desired quality standards. Verification, validation, and testing are
the means to accomplish these goals.        

3. The software project management
    - Software project management activities ensure that the software system under 
development will be delivered on schedule and within the budget constraint.        

3. The OOSE, object oriented software engineering
    - OOSE views the world and systems as consisting of objects that relate to
and interact with each other.      

4. The OO and conventional
    - First, maintaining numerous conventional systems is required. 
    - Second, numerous organizations still use the conventional approaches. 
    - Third, a conventional methodology may be more appropriate for some projects such as scientific computing. 
    - Finally, a system may consist of components developed by conventional and OO approaches.        

5. Software engineering and computer science
    - Computer science disciplines such as algorithms and data structures, database systems, artificial intelligence, operating systems, and others emphasize computational efficiency, resource sharing, accuracy, optimization, and performance. These attributes can be measured quantitatively and immediately.      
    - Software engineering would use a good-enough solution to reduce development time and effort. Efforts and resources spent in software engineering R&D are aimed at improving software PQCT.      
    - They are very related.      
    - CS is more related to academic. The real worl project is larger and more complex. CS is not enough and we should learn SE. 

6. The project challenge of system development
    - The project reality, require long development duration, require many years to develop.        
    - The project challenge, how to schedule.
    - The project reality, collaboration.      
    - The project challenge, how to divide work and integrate the component.       
    - The project reality, people are allowed to different tool. People are in different building and different location.      
    - The project challenge, how to ensure communication and coordination.        

7. The product challenge of system development
    - The product reality, requirement and constraint.
    - The project challenge, how to ensure that these requirements and constraints are met.
    - The product reality, requirement and constraint will change.
    - The project challenge, how to cope with change.
    - The product reality, maintenance.
    - The project challenge,  maintenance challenge.

8. The softwaore process
    - The series of phases of activities performed to cinstruct a software system.









