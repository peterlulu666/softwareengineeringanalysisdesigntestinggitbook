## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1okus58outoMJzLXF2cvfW-RHsv-EaRR3/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1NKyRxJwbMMf59Qk0_mDeFB4IvRK_87kd/preview"></iframe>

## Quiz      

1. The use case modeling step
    - The planning phase
        - The **requirements**.
        - The **deriving** use cases from requirements.
        - The **abstract** use cases.
        - The defining use case **scope**.
        - The **abstract** & **high level** use cases.
        - The depicting use case contexts.               
    - The iterative phase
        - Specifying actor system interaction.        

2. The deriving use case from the requirement
    - In the requirements specification, look for “verb-noun” phrases that indicate domain specific.
        - “do something”.
        - “something must be done”.
        - “perform some task”.
        - in the application domain.
    - Verify the “verb-noun” phrases using use case definition.

3. Verify the use case identified
    - Verify the use cases identified using use case definition: 
        - Is it a business process? y/n
        - Is it initiated by an actor? y/n
        - Does it end with the actor? y/n
        - Does it accomplish something useful for the actor? y/n
    - All the answers to above questions must be “y”.

4. The identify actor, system or subsystem
    - From the requirements, identify also
        - The actors, who initiate the tasks or for whom the tasks are performed.
        - The system or subsystem that contains the use case.

5. The requirements of a library system
    - R1. The library system must allow a patron to check out documents.
    - R2. The library system must allow a patron to return documents.
6. The use case diagram
    - The actor.
    - The system name.
    - The use case.
    - The system boundary.
    - The association.

7. The requirements of the overseas exchange program
    - R1. The web-based application must provide a search capability for overseas exchange programs using a variety of search criteria. 
    - R2. The web site must provide a hierarchical display of the search results to facilitate user navigation from a high level summary to details about an overseas exchange program.

8. The business process, step, operation, and action
    - A **business process** is a series of **steps** to accomplish a complete business task.
    - An **operation** is a series of **actions** or instructions to accomplish a step of a business process.

9. The three level of abstraction use case 
    - Abstract use case: a “verb-noun” phrase. 
    - High level use case: when and where the use case begins and when it ends.
        - TUCBW, This use case begins with. 
        - TUCEW, This use case ends with. 
    - Expanded use case: step-by-step description of how the actor interacts with the system to carry out the business process. 

10. The high level use case 
    - A high level use case should not specify background processing. 
        - Do not specify how the system processes the request such as update the database. 
            - Background processing is modeled by sequence diagrams developed later. 
    - High level use cases should end with what the actor wants to accomplish. 

11. The requirement 
    - Traceability addresses the following 
        - How do you know that the system will deliver all the
capabilities stated in the requirements?
        - How do you know to what extent the system will satisfy the requirements?
        - How do you know if some use cases are missing?
        - How do you know which use cases are not needed?
        - How do you know which requirements are more important than the others?
        - How do you know which use cases should have high priority?

12. The Traceability matrix
    - It highlights which use cases relate to which requirements, and vice versa. 
    - It shows the **priorities** of the requirements and use cases. 
        - Uses scoring mechanism. 
        - Projects should focus on timely delivery of high-priority use cases first. 
    - It is useful for use case based acceptance testing. 
        - High-priority use cases should be tested first. 
    - Why prioritize use cases with requirements?
        - Customers can **prioritize** requirements. 
        - Developers can relate use case priority directly with requirements priority. 
        - If we build highest priority use cases first, followed by second priority, followed by . . . , then customers see most important requirements first, second, etc. in the agile release sequence. 
            - Helps keep customer **engaged** with development team. 
            - If time or money becomes limited, most important requirements/UCs are done first. 

13. The project planning by the use case 
    - Identify dependencies between the use cases. 
        - Use cases are business processes. 
        - Business processes **depend** on each other. 
            - Cannot return a book unless it had been checked out. 
    - Compute a partial priority order to develop the use cases according to the **dependencies**. 
    - Schedule the development according to the partial **priority** order. 
        - Favor higher priority use cases when no ordering exists between two use cases. 
    - A planning tool to let the development team know. 
        - which use cases are to be developed by what time.
        - which use cases are ready for integration and system testing. 
        - what is the status or configuration of the project. 

14. Applying agile principle
    - Work closely with customer and users to understand **their** business **processes and priorities** and help them identify their real needs.
    - The team members should work together to identify use cases, actors, and subsystems and specify the use case **scopes**.
    - Requirements **evolve** but the timescale is **fixed**.
    - Focus on frequent delivery of small **increments**, each delivery deploys only a couple of use cases.
    - Do not attempt to derive an **optimal** set of use cases. Good enough is enough. 

15. The software **architecture** of a system or subsystem refers to the style of structural design including the interfacing and interaction among its subsystems and components. 
    - **Components**, **Connectors**, and **Constraints**. It is the 3C. 

16. Different types of systems require different design methods and
architectural styles. 
    - Affects system properties of utility, efficiency, security, safety, and others. 

17. Architecture plays a central role for the entire life of the software system. 

18. What is the software architecture design 
    - The software architecture of a system or subsystem refers to the style of design of the system structure including the interfacing and interaction among its major components.
    - The Software Architectural Design activity is a decision-making process to determine the software architecture and its representation for the system under development.
    - According to IEEE 1471-2000, Recommended Practice for Architectural Description of Software Intensive Systems. 
        - Software architecture is the fundamental organization of a system, embodied in
            - Thecomponents of the system.
            - Relationships (connections) among the components.
            - Relationships (connections) between the components and the environment.
            - Principles (constraints) governing the design and evolution of the system. 

19. The importance of the architectural design 
    - The architecture of a software system has significant impact on performance, efficiency, security, and maintainability. 
    - The Architectural Representation is the primary artifact for conceptualization, constructing, managing, and evolving the system under development. 
        - And later, the system under maintenance. 
    - Architectural design flaws (or even specific ‘choices made’) could result in project failure. 
        - Major cost and schedule over-runs.
        - Lost functionality.
        - Project being abandoned. 

20. The architectural design
    - Adapt an architectural style when possible.
    - Apply software design principles.
    - Apply design patterns.
    - Check against design objectives and design principles.
    - Iterate the steps, if needed. 

21. The architectural design consideration
    - Ease of change and maintenance.
    - Use of commercial off-the-shelf (COTS) parts.
    - System performance. 
        - Does the system require processing of real-time data or a huge volume of transactions? 
    - The non functional requirements
        - Reliability.
        - Security.
        - Software fault tolerance.
        - Recovery. 

22. The system and the behavior, the type fo the system
    - Interactive subsystem. 
    - Event-driven subsystem. 
    - Transformational subsystem. 
    - Database subsystem. 

23. Interactive subsystem 
    - The **interaction** between system and actor consists of a relatively fixed sequence of actor requests and system responses.
        - The system has to process and respond to each request.
    - Often, the system interacts with only one actor during the process of a use case.
        - The actor is often a human being although it can also be a device or another subsystem.
    - The interaction begins and ends with the actor.
    - The actor and the system exhibit a “client-server” relationship.
    - System state reflects the progress of the business process represented by the use case. 

24. Event-driven subsystem 
    - An Event-Driven System receives events from and controls external entities.
        - It does not have a fixed sequence of incoming requests .
        - Requests arrive at the system randomly.
        - It does not need to respond to every incoming event.
    - Its response is state dependent.
        - The same event may result in different **responses** depending on system state.
    - It interacts with more than one **external entity** at the same time.
    - **External entities** are often hardware devices or software components rather than human beings.
    - Its state may not reflect the progress of a computation.
    - It may need to meet **timing** constraints, **temporal** constraints, and **timed temporal** constraints. 

25. Transformational subsystem 
    - Transformational systems consist of a network of information-processing activities.
        - Transforming activity input to activity output.
    - Activities may involve control flows that exhibit sequencing, conditional branching, parallel threads, and synchronous and asynchronous behavior.
    - During the transformation of the input into the output, there is little or no interaction between system and actor.
        - It is a batch process.
    - Transformational systems are usually stateless.
    - Transformational systems may perform number crunching or computation-intensive algorithm.
    - The actors can be human beings, devices, or other systems. 

26. Database subsystem, object persistence system 
    - An Object Persistent System provides capabilities for storing and retrieving objects from a database or file system while hiding the storage media.
        - It provides object storage and retrieval capabilities to other subsystems.
    - It hides the implementation from the rest of the system.
    - It is responsible only for **storing** and **retrieving** objects and does little or no business processing except for performance considerations.
    - It is capable of **efficient storage**, retrieval, and updating of **a huge amount** of structured and complex data. 

27. The system and the subsystem 
    - It is worth mentioning again here.
    - Systems and subsystems are relative to each other.
        - A system is a subsystem of a larger system.
        - A subsystem is a system and may consist of other subsystems.
    - A system may consist of different types of subsystems.
    - The design method applied needs to match the type of subsystem under development. 




















