# Summary

* [Introduction](README.md)
* [Lecture 1](lecture1.md)
* [Lecture 2](lecture2.md)
* [Lecture 3](lecture3.md)
* [Lecture 4](lecture4.md)
* [Lecture 5](lecture5.md)
* [Lecture 6](lecture6.md)
* [q1](q1.md)
* [Quiz 1](Quiz1.md)
* [Lecture 7](lecture7.md)
* [Lecture 8](lecture8.md)
* [Lecture 9](lecture9.md)
* [Lecture 10](lecture10.md)
* [Lecture 11](lecture11.md)
* [q2](q2.md)
* [Lecture 12](lecture12.md)
* [Lecture 13](lecture13.md)
* [Lecture 14](lecture14.md)
* [Lecture 15](lecture15.md)
* [q3](q3.md)
* [Lecture 16](lecture16.md)
* [Lecture 17](lecture17.md)
* [Lecture 18](lecture18.md)
* [Lecture 19](lecture19.md)
* [Lecture 20](lecture20.md)
* [Quiz 4](Quiz4.md)




























