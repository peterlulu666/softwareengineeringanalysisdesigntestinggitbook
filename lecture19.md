## Lecture     

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1ZAGzaJS6x8DvIoynMhov79Xi27Sh3uDB/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/128qUTMoyOqK2qaaU4ULE7-UwRMFeVVwh/preview"></iframe>

## Quiz      

1. Formal Verification
    - A precise **mathematical** analysis process 
    - It  checks the **correspondence** between a specification and an implementation 
    - Example: 
        - **specification**: a logical expression of some constraint – bad things should not happen 
        - **implementation**: a state machine model representing the design of a system 
        - **formal verification**: model checking to find a state that violates the logical expression

2. Informal Verification
    - The correspondence is usually checked by a **manual process** 
    - Techniques include **desk-checking**, **inspection**, **walkthrough**, and **peer review**

3. Verification Is Consistency Checking
    - Internal consistency: a specification must **not** contain a **contradiction** 
    - Consistency between two levels of **abstraction**: 
        - software design specification and the requirements specification 
        - leveling in Data Flow Diagram decomposition 
    - Consistency between a **product** and **standards**

4. V&V in the Requirements Phase
    - Activities performed during the requirements phase include: 
        - technical review, expert review, and customer review 
        - prototyping 
        - inspection, and walkthrough 
        - formal and informal verifications 
        - requirements tracing

5. Technical Review
    - Technical reviews are performed internally by developers looking for: 
        - **incomplete** definitions 
        - **incomplete** formulation of conditions such as an if- condition without an else part, or less than 2**n rules for n binary conditions 
        - **inconsistencies** in requirements, types, and logical expressions 
        - **ambiguities** in definitions and requirements
        - **duplicate** formulations of concepts, requirements, or constraints 
        - requirements **traceability** problems 
        - **unwanted** implementation details, e.g., 
            - use of pointers, defining physical data structures 
            - use of pseudo-code or programming language code 
        - potential **feasibility**, **performance**, or **cost** problems

6. Checking Requirements and Constraints
    - Definition completeness 
        - every **concept** mentioned in the requirements has an explicit **definition** 
        - pay attention to important but **undefined** adjectives (e.g., good customer receives 20% discount) 
        - every defined **concept** has been used**at least once**
    - Type **consistency** 
        - types of objects involved in each concept match the **definition** and each use of the **concept** 
    - Logical completeness and **consistency**

7. Ambiguity in Requirements
    - Completeness: all cases are covered 
        - external completeness, a validation problem 
        - internal completeness, a verification problem 
    - Consistency: there exists no contradiction, the system is theoretically possible 
    - Unambiguity: there exists at most one possible interpretation of the requirements
    - <iframe height="842" width="100%" src="img/Ambiguity in Requirements.pdf"></iframe>

8. Expert Review
    - Expert reviews are performed by domain experts, who look for . . .
        - inaccuracies in the formulation of domain concepts, regulations, policies, standards 
        - conceptualization problems 
        - incorrect formulation of business rules 
        - other domain specific problems

9. Customer Review
    - These reviews are performed with customer representatives and users, and look for . . .
        - mismatch between **requirements** and real world 
        - problems in user interfaces such as sequence of interaction, look and feel, use of **GUI** implementation technologies 
        - issues relating to **non-functional** requirements 
        - problems in application specific **constraints** such as operating environment, costs, political considerations

10. Requirements Tracing
    - Performed by the authors of the **requirements** prior to external reviews 
    - Ensure that each higher-level requirement **corresponds** to some lower-level requirements
    - Ensure that each lower-level requirement **corresponds** to some higher-level requirement 
    - Ensure that lower-level requirements are sufficient to **realize** the higher-level requirements

11. V&V in the Requirements Phase
    - V&V in the requirements phase also check the requirement-use case traceability matrix 
        - ensure that each requirement is realized by some use cases, and 
        - each use case realizes some requirements 
    - Use prototyping to demonstrate capabilities of the system to the customer representatives and user representatives

12. V&V of Architectural Design
    - **Activities**: 
        - peer design review 
        - design inspection 
        - design walkthrough 
    - **Check** for design quality metrics 
    - **Check** to ensure that requirements and constraints are fulfilled 
    - **Check** to ensure software design principles, and security principles are satisfied

13. Desirable Properties for Architectural Design
    - **Subsystems** and modules should be functionally cohesive 
    - Interaction between **subsystems** and modules should be explicit 
    - Modules should be small and easy to **understand** 
    - **Decisions** should be confined to a single module 
    - Modules should be easy to test and **maintain** 
    - **Implicit**, **global assumptions** should be avoided

14. V&V in the Implementation Phase
    - **Activities**: 
        - detailed design review 
        - code review, inspection, and walkthrough 
        - testing, and 
        - reliability assessment 
    - **Check** 
        - **correspondence** between the implementation and design 
        - **consistency** of module interfaces
        - **correctness** of the implementation 
        - **conformance** of coding standards 
        - **proper use** of programming constructs 
        - **un-initialized**/improperly initialized variables, structures or pointer 
        - quality of the **code** according to various code quality metrics 
            - cyclomatic complexity (must not exceed 10) 
            - information hiding 
            - cohesion and coupling 
            - modularity 
            - etc.

15. Software Quality Assurance Functions
    - **Definition** of Processes and Standards 
        - **Definition** of Process and Methodology 
        - **Definition** of SQA Standards and Procedures 
        - **Definition** of Metrics and Indicators 
    - **Quality** Management 
        - **Quality** Planning 
        - **SQA** Control 
    - **Process** Improvement

16. Definition of Processes and Standards
    - **Definition** of a SQA Framework 
        - **defining** development, quality assurance, and management processes and methodologies (see Figure 19.5 for SQA in plan-driven and agile processes) 
        - **defining** SQA standards, procedures, and guidelines 
        - **defining** quality metrics and indicators for quality measurement and assessment

17. Quality Management Planning
    - Quality planning defines a scheduled sequence of activities to be carried out to assure the desired software quality 
    - Examples are ANSI/IEEE Standard 730-1984, and 983-1986 SQA Plans

18. Components of an SQA Plan
    - **Purpose** - objectives and scope of the plan as well as the product and its use 
    - **Management** – project organization, and team structure including roles and responsibilities for SQA functions and activities 
    - **Standards and conventions** to be applied 
    - **Review and audit** – types of reviews to be used, problem reporting, and correction procedures
    - Software configuration management 
        - activities to ensure consistent update, and track changes 
    - Processes, methodologies, tools, and techniques to be used 
    - Metrics and indicators to be applied

19. Watts Humphrey’s Quality Plan Outline
    - **Product introduction** 
        - A description of the product, its intended market, and  the quality expectations for the product 
    - **Product plans** 
        - The critical release dates and responsibilities for the product along with plans for distribution and product servicing 
    - **Process descriptions** 
        - The development and service processes which should be used for product development and management 
    - **Quality goals** 
        - The quality goals and plans for the product, including an identification and justification of critical product quality attributes 
    - **Risks and risk management** 
        - The key risks which might affect product quality and the actions to address these risks

20. SQA Control
    - It **ensures** that the SQA plan is carried out correctly 
    - It **monitors** the software development project 
    - It **collects** and **manages** SQA related **data** 
    - It **reports** the data to software process **improvement** process

21. Process Improvement Process
    - Definition and execution of a process **improvement** process (PIP) 
    - It defines metrics and indicators for process **improvement** 
    - It gathers data for **improving** the process 
    - It **computes** the metrics and indicators 
    - It produces process **improvement** recommendation to the management

22. Applying Agile Principles
    - Good enough is **enough** 
    - Keep it **simple** and **easy** to comply 
    - Management **support** is essential 
    - A **collaborative** and **cooperative** approach between all stakeholders is essential 
    - The team must be **empowered** to make decisions 
    - Values working software over **comprehensive** documentation

23. Software Quality Assurance
    - Discussed what software quality was and the benefits of establishing quality efforts in software 
        - Software **Quality** attributes, measures, metrics, and indicators 
        - “**Cost** of Quality” 
        - Example software quality **measures** 
    - Discussed software V&V techniques 
        - Inspections, walkthroughs, peer reviews 
        - Lifecycle opportunities with V&V 
    - Discussed the role and functions of SQA 
        - Processes, standards 
        - Methodologies 
        - Planning, control, metrics, process improvement

24. Software Testing
    - Software testing is a dynamic **validation** technique 
    - Software testing can **detect** errors in the software but cannot prove that the software is free of errors 
    - Software testing increases the development team’s **confidence** in the software product

25. Software is Ubiquitous
    - **Everywhere**! – Automotive, phones, industrial control, power distribution, embedded devices, aerospace, military, IT systems 
    - Software is used in **high security** and/or safety applications 
        - Medical devices, nuclear reactors, aerospace (commercial and military) 
        - Banking Systems, identification systems, surveillance systems 
    - The user expectation that software will work reliably places important emphasis on **software test**

26. Other Motivations for Software Test
    - **Agile** processes emphasize test 
        - Test driven requirements (user stories) 
        - Continual unit testing 
    - The Industry has created a number of Software Test **Certification** organizations 
        - Some companies highly value these **certifications** $$ 
        - Much of the class material this semester will be aligned to the ISTQB (International Software Testing Qualifications Board) foundation level **material** and **test** (more later) 
    - As good as the CMMI is, it is particularly weak in the software **test** function 
        - It does address many verification techniques but does not address common methods of **software test**, **estimation**, **management** or **planning** 
    - Most CS degree holders graduate without a single class in software **test**

27. Cost of Late Testing
    - <img src="img/Cost of Late Testing.png" alt="Cost of Late Testing" width="500" height="600">

28. Definition off Terms
    - Definitions are from the IEEE 24765 and the ISTQB definitions when they agree **no color** is used - These terms are used throughout the software literature 
    - Bug: see Defect 
    - Defect: see Fault see Fault 
    - Error: “A human action that produces an incorrect result.” This a **mistake** that a **human** makes 
    - Fault: “An **incorrect** step, process, or data definition in a computer** program**” “A flaw in a component or system that can cause the component or system to fail to perform its required function” 
    - Failure: “Termination of the ability of a product to perform a required function or its **inability to perform** within previously specified limits.” “Deviation of a component or system from its expected delivery, service, or result” 
    - Mistake: see Error 
    - Latent Defect: An **undiscovered defect** in delivered software 
    - Test Oracle: “A source to determine **expected results** to compare with the **actual result**  of the software under test."

29. A Better Understanding of the Terms
    - Failure is what is observed “from the outside” – by the customer or other **outside** viewer 
    - A Failure is caused by one or more faults (an incorrect step or flaw) in the software – a fault is also known as a **defect** or bug – this is what we detect through **test** or **inspection** 
    - An Error – this is the **mistake** 
    - A good analogy is the following event 
        - A human has a heart attack 
        - The failure - the heart’s ability to supply oxygen rich blood to the body - this can be observed 
        - The fault - the heart muscle stops working because of oxygen starvation - one or more coronary arteries have become blocked 
        - The error (mistake) – (probably) Poor diet - how do you spell “Supersize Me”? 
        - Not all heart attacks are caused by the same faults or errors 
        - Poor diet does not always cause a heart attack – but it is still there with the potential to cause a fault . . . which could cause the failure 
        - Like defect detection above (test/inspection), the fault is fixed with medicine but the error (poor diet) may still be there

30. Deffect Detection and Removal
    - V&V is a **defect** (fault) **detection** activity - it is **not** a **failure** **detection** activity 
    - The term ‘failure’ is an **affect** **not** a **cause**. One or more **defects** are the **cause** of the failure. This means that the same failure may still result when one of its contributing faults is fixed 
    - An understanding of defects and removal is important 
        - Defects are detected -> some or all detected defects are removed – the removal only occurs when the product is corrected 
        - Defect removal is a function of both detection and removal - most software has more defects detected than removed – why? 
        - **Un-removed defects** are called ‘restrictions’ 
        - **Un-detected defects** are ‘latent defects’ 
        - Almost all software has restrictions and latent defects 
    - We can predict both faults and failures in the software – the latter requires **operational** history

31. Verification/Validation Activities and the SDLC
    - <iframe height="842" width="100%" src="img/Verification/Validation Activities and the SDLC.pdf"></iframe>

32. Software Testing and Techniques Are Essential
    - Technical reviews can **detect** a significant percentage of **defects**, but it is important to understand what kinds of defects they detect 
        - Defects are typically limited to the scope of the product being reviewed or possibly close neighbors 
        - Defects are typically limited to those found statically (as opposed to dynamically such as execution)
    - Example: for a “group” of **requirements** we would normally find the following during a technical review 
        - Defects related to the **requirements** being reviewed (or possibly closely related requirements) 
        - Issues with the **completeness** or **correctness** of specification (missing/incorrect thresholds, logic, sequencing, etc.) 
    - Defects that are related to dynamic or more global functionality are best suited for **testing** - the need for software **testing** will never be eliminated! 
    - Software test design 
        - Where most **defects** are **detected** – not during test execution 
        - Techniques are very useful for **technical** reviews also

33. A Few More Introductory Terms
    - Quality: “The degree to which a component, system or process meets specified **requirements** and/or user/customer needs and **expectations**.”
    - Defect prevention: “A structured problem-solving methodology to identify, analyze and **prevent** the occurrence of **defects**.” Defect prevention changes the process to prevent occurrence 
        - How does this compare with defect detection and removal? 
        - What is quality control vs. quality assurance (defect prevention)? 
    - Debugging: “The process of **finding**, **analyzing** and **removing** the causes of failures in software.” 
        - How does debugging differ from defect detection? 
        - From troubleshooting?

34. Seven Testing Principles
    - **Testing shows presence of defects**: Testing can **show** the **defects** are present, but **cannot prove** that there are **no defects** 
    - **Exhaustive testing** is **not possible** 
    - **Early testing**: In the software development life cycle testing activities should start as **early** as possible to **reduce cost** 
    - **Defect clustering**: A small number of modules contains most of the **defects** discovered. Sometimes referred to as the 80/20 rule 
    - **Pesticide paradox**: If the same kinds of **tests** are **repeated** again and again, eventually the same set of test cases will **no longer** be able to **find** any new **bugs**. (Does not apply to regression testing.) 
    - **Testing is context-dependent**: Different software products have **varying** requirements, functions, and purposes 
        - For example, safety–critical software is tested differently from an e-commerce site 
    - **Absence-of-errors fallacy**: Declaring that “a test has **unearthed no errors**” is **not** the same as declaring the software “**error-free**”

35. Test Case Generation- White Box
    - Knowing the **internal** workings of a product 
        - focus on the program’s **structure** and **internal** logic 
        - test cases are designed according to the **structure** and **internal** logic 
    - Well-known **techniques** 
        - **Basis path testing**: test cases are designed to exercise the control flow **paths** of a program 
        - **Condition testing**: test cases are designed to exercise each outcome of a **condition** 
        - **Data flow testing**: test cases are designed to test **data** elements’ define and use relationships

36. Test Case Generation-back Box
    - Knowing the **functional** specification 
        - Focus on **functional** requirements 
        - Test cases are designed to test the **functionality** of the program 
    - Well-known techniques: 
        - Boundary value analysis -- test cases are derived according to the boundary **values** of variables 
        - Causal-effect analysis -- test cases are derived according to the stimuli and responses, and **input** **output** relationships 
        - Equivalence partitioning -- test cases are derived from partitions of the **input** and **output** domains

37. Test categories
    - **Unit Testing**: **testing** at the **individual** module level, functions, modules, classes, etc. 
    - **Integration Testing**: **testing** the **interfaces** among the modules or components 
    - **Validation Testing**: **test** according to customer’s **functional** requirements 
    - **System Testing**: 
        - Recovery Testing 
        - Security Testing 
        - Stress Testing 
        - Performance Testing

38. Black Box Testing Techniques
    - Typical “black-box” test analysis and design techniques include: 
        - Equivalence partitioning 
        - Boundary value analysis 
        - Decision table testing 
        - State transition analysis 
        - Use Case testing – Decision logic and Karnaugh maps 
    - Black box is a bit of a misnomer – we do not and cannot test strictly black box 
    - So these are correctly called Specification-based test analysis and design techniques, but we will use the term “black box” because it is commonly used

39. Equivalence Classes or Partitions
    - This is a technique used to **reduce** a large **input** space down into a more manageable set 
    - I have a requirement to set an alarm when the temperature (assume integer) is greater than 50 degrees F, otherwise the alarm is not silenced
    - I have two **equivalence** classes based on the actions needed – they are **equivalent** because the actions taken are **equivalent** within the same **partition** 
    - A domain **partition** is a **partition** of the input domain into a number of **sub- domains** 
    - A boundary is where two **sub-domains** meet
    - A savings account in a bank has a different rate of interest depending on the balance in the account
        - 3% rate of interest is given if the balance in the account is less than $100 
        - 5% rate of interest is given if the balance in the account is in the range of $100 to $1000 
        - 7% rate of interest is given if the balance in the account is at least $1000
    - We need to pay careful attention to the range of values 
    - What would happen to the ranges above if this was float but not dollars and cents? How would this change when we represent this in code? 
    - Here we have three **partitions** based on the set of actions

40. Boundary Value Analysis
    - **Boundary** value testing does two things – It drastically **reduces** the number of **test** cases needed – It accounts/checks for the **human** tendency to have  specification **errors** at boundary conditions
    - We set **inputs** based on each “border” of the **partition** and check actual **outputs** to the expected **condition**
    - We will have a **minimum of two** test cases

41. Decision Tables
    - If you are a new customer and you want to open a credit card account then there are three condition 
        - You will get a 15% discount on all your purchases today 
        - If you are an existing customer and you hold a loyalty card, you get a 10% discount 
        - If you have a coupon, you can get 20% off today (but it can’t be used with the ‘new customer’ discount). discount amounts are added, if applicable 
    - Some interesting combinations… 
        - What happens if I am a new customer? Obvious! 
        - What happens if I am a new customer with a coupon? 
        - What happens if I am an existing customer with a coupon and a loyalty card?
        - What happens if I am a new customer with a loyalty card and no coupon? 
        - What happens if I am a new customer with a loyalty card and a coupon?
    - We can make this much more concise and understandable 
    - How many conditions do we have? How many possible combinations does that give us? 
    - How many actions do we have?
        - <img src="img/Decision Tables.png" alt="Decision Tables" width="500" height="600">
    - I have two cases where the requirements are not complete. I need to update the requirements to address what action I do with these 
    - The best way to test this is to develop a test case for each column
    - Loan Calculation
    - The input has four fields and a Submit button, you can enter 
        - The amount of the total loan 
        - Interest rate 
        - The number of months you want to pay it off (term) 
    - Rules 
        - The monthly payment is calculated when the submit button is pressed if: 
        - All three are given 
        - Based on 5.5 % interest if the total loan and term is given 
        - Based on 48 months term if the total loan and interest are given 
        - An error condition is indicated: 
        - When the total loan amount has not been given
    - First we do not list the Submit button as an action – why? 
    - Second, the requirements are incomplete – a condition in the decision table has not been specified
    - Now the requirements are complete – all conditions in the decision table have been specified























































































