## Lecture      

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1e3ix-qIO0s41FVDrjwJu8M8RMK7Jl_IY/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/19pYTKxiDqNIJC71c3C-rmJDtcjGhbP6G/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1C3UfurW3kMDO379tpAMeB-2QLpyCOB7b/preview"></iframe>

## Quiz        

1. The Information Expert
    - Clearly, the object that is responsible for handling a **request** should have the information to fulfill the request
    - As indicated below, the **request** to verify whether a password is valid should be assigned to a user object because password is one of its attributes
    - <img src="The Information Expert.png" alt="The Information Expert" width="500" height="600">
    - If this responsibility were assigned elsewhere something else would have to query the user object to get the password and determine its validity or it would have to ask the user object to check the validity – both introduce unwanted coupling
    - The (information) Expert Pattern assigns the responsibility to handle the request to the object that has the information to fulfill the request

2. A Login Sequence Diagram

3. Multiple Applications of Expert Pattern
    - Often, more than one expert needs to collaborate to fulfill a request 
    - In the following sequence diagram, this is accomplished within the constructor of the Loan class 
        - create (p: Patron, d: Document) in the UML notation 
    - However, note that if the due date depends on the following, we need to compute different due dates: 
        - The kind of patron (faculty, staff, or student) 
        - The kind of document (book or reserve) 
    - For this the request to compute the due date involves at least three objects: the patron, the document, and the calendar 
    - Since the Loan object has access to all these objects that have the needed information, it is appropriate to assign this responsibility to the Loan object

4. Applying The Expert Pattern
    - <img src="img/Applying The Expert Pattern.png" alt="Applying The Expert Pattern" width="500" height="600">

5. The Expert Pattern
    - It is a basic **guiding principle** of OO design
    - ~70% of responsibility assignments apply the Expert Pattern
    - It is frequently applied during object **interaction** design –constructing the **sequence** diagrams
    - The Expert Pattern has the following **benefits**
        - Low coupling 
        - High cohesion 
        - Easy to comprehend and change 
        - Tend to result in “stupid objects”

6. Class exercise
    - <img src="img/Class exercise.png" alt="Class exercise" width="500" height="600">
    - <img src="img/A Reset Password Sequence Diagram.png" alt="A Reset Password Sequence Diagram" width="500" height="600">
    - Problems with the Design
    - It assigns getQuest() and checkAns() to the wrong object –DBMgr, which does not have the attributes to fulfill the requests
    - It does not design “stupid objects”
    - It violates the Expert Pattern responsibility assignment
    - It is designed with a “conventional mindset”
    - <img src="img/Applying The Expert Pattern reset password.png" alt="Applying The Expert Pattern reset password" width="500" height="600">

7. The Creator Pattern
    - <img src="img/The Creator Pattern.png" alt="The Creator Pattern" width="500" height="600">
    - Object **creation** is a common activity in OO design
        - It is useful to have a general **principle** for assigning the responsibility to **create** an object
    - Assign class B the responsibility to create an object of class A if
        - B is an **aggregate** of A objects. The book is the aggregate of the chapter. The book create teh chapter
        - B contains A objects, for example, the dispenser contains vending items
        - B records A objects, for example, the dispenser maintains a count for each vending item
        - B closely uses A objects
        - B has the information to create an A object
        - <img src="img/The Creator Pattern Book Chapter.png" alt="The Creator Pattern Book Chapter" width="500" height="600">
        - <img src="img/The Creator Pattern Book Portfolio.png" alt="The Creator Pattern Book Portfolio" width="500" height="600">
        - <img src="img/The Creator Pattern Checkout.png" alt="The Creator Pattern Checkout" width="500" height="600">
        - Benefits of Creator Pattern
            - Low coupling because the coupling already exists
            - Increases reusability
            - Related patterns
                - Low coupling
                - Creational patterns (abstract factory, factory method, builder, prototype, singleton)
                - Composite

8. Design Patterns Chapter
    - Design Patterns are proven design solutions to **commonly encountered design problems**
        - Each pattern solves a class of design problems
    - Design Patterns presented here address a **common design problem**
        - How does one assign responsibilities to objects?
    - Questions to answer
        - Who should be assigned the responsibility to handle an **actor** request
        - Who should be assigned the responsibility to handle a request in the **business** object layer
        - Who should be assigned the responsibility to **create** a given object
    - Design Patterns presented
        - Singleton - design a class that has only one globally accessible instance
        - Controller - assign the responsibility for handling an actor request to a controller
        - Expert – assign the responsibility for handling a request to the object that has the information to fulfill the request
        - Creation – the responsibility to create an object should be assigned to the object that is an aggregate of, depends on, or otherwise has the information to create the given object
    - Each pattern has a design pattern specification
        - Addresses the problem, solution, design, roles and responsibilities , benefits, liabilities, and uses in a standard format
        - Design Patterns presented should help students understand
            - What pattern to apply and how to apply them
            - What pattern, when to apply them, and why

9. Deriving a Design class Diagram
    - A Design Class Diagram (DCD) is a UML class diagram derived from the behavioral models and the domain model
    - The DCD serves as a design **blueprint** for test-driven development, integration testing, and maintenance
    - There is **only 1** DCD for the whole system
    - Package diagrams are useful for organizing and managing the classes of a large DCD

10. Deriving Design Class Diagram
    - A Design Class Diagram (DCD) is a **structural diagram**
        - It shows the **classes**, their **attributes** and **operations**, and **relationships** between the classes
        - It may also show the **design patterns** used
        - It is used as a basis for **implementation**, **testing**, and **maintenance**
    - The collection of the **domain model and sequence diagrams** does **not** provide a class **structure** and **road-map** to guide subsequent efforts
        - Information is contained in these diagrams, but is **scattered** across them
    - The DCD provides this **‘design specification’** of the classes and the class **structure** is the DCD
        - There is **only one** DCD for the system
        - We add to this one DCD based on what is captured in each iteration

11. Steps Used to Derive the DCD
    - Identify Classes (from the Design Sequence Diagrams for the current iteration)
    - Identify Methods (from the Design Sequence Diagrams for the current iteration)
    - Identify Attributes (Design Sequence Diagrams and Domain Model)
    - Assess relationships between classes and their implications

12. Steps for Deriving Dcd-step 1
    - **Identify all classes** used in each of the sequence diagrams and put them down in the DCD
        - **Identify classes** of objects that **send or receive messages**
        - **Identify classes** of objects that are passed as **parameters or return types/values**
            - Messages between objects represent function calls from one object to another (the next slide shows d:Document, and l:Loan are parameters, so these are identified)
        - Identify classes that serve as **return types**
            - In the next slide, ‘Document’ is the only class used as a return type
        - **Identify classes** from the **domain model**
            - Classes from the domain model that are parent classes may be added to the DCD is it promotes understanding
        - Identify Classes Used in Sequence Diaarams
            - <img src="img/Identify Classes Used in Sequence Diaarams.png" alt="Identify Classes Used in Sequence Diaarams" width="500" height="600">
        - Casses Identified
            - <img src="img/Casses Identified.png" alt="Casses Identified" width="500" height="600">
        - When identifying these classes keep in mind the following
            - These rules should be applied to **all design sequence diagrams** produced in the current iteration, **not** to just **one of the sequence diagrams**
            - A **class** may be identified by **more than one rule** - each class will only be **identified once**
            - Since the DCD serves as the design blueprint for the system, **only one** DCD will capture all the behavioral models - **not** one per sequence diagram or each iteration

13. Steps for Deriving DCD- Step 2
    - **Identify methods** belonging to each class and fill them in the DCD
        - In a design sequence diagram, a message x:=m(...):X that is sent from object A to object B indicates that A calls the m(...):X function of B and saves the result in variable x of type X
            - We use this to identify all such methods for each class
            - Methods are identified by looking for messages that label an incoming edge of the object
        - The sequence diagram may also provide detailed information about the parameters, their types, and return types
    - Identify Methods
        - <img src="img/Identify Methods.png" alt="Identify Methods" width="500" height="600">
    - Fil In Identified Methods
        - <img src="img/Fil In Identified Methods.png" alt="Fil In Identified Methods" width="500" height="600">

14. Steps for Deriving Dcd-step 3
    - **Identify** and fill in **attributes** from sequence diagrams and the domain model
        - **Identify attributes** from methods that retrieve **objects**
            - A message of the form getX(a:T):X, where X is a class and T is a scalar type, suggests that a is an attribute of X and the type of a is T
        - **Identify attributes** from the normal **get** and **set** methods
        - **Identify attributes** from isX():**boolean** and setX(b:boolean) methods, where X is an adjective
            - Names such as ‘is’ and ‘set’ often imply attributes .vs. methods
        - **Identify attributes** from methods that compute a **scalar type value**
            - A message of the form computeX(...) or x:=computeX(...):T may suggest that X is an attribute of the object that receives the message
            - The type of that attribute would be T
        - **Identify attributes** from the parameters to a **constructor**
            - A message create(callNo: String, title: String...) the parameters are typically attributes
        - **Attributes are identified** from the **domain model**
            - Compare each DCD class with the domain model and identify attributes that are needed by the operations of the DCD
    - Identify Attributes
    - <img src="img/Identify Attributes.png" alt="Identify Attributes" width="500" height="600">
    - Fill In Attributes
    - <img src="img/Fi In Attributes.png" alt="Fi In Attributes" width="500" height="600">

15. Steps for Deriving Dcd-step 4
    - **Identify** and fill in **relationships** from sequence diagrams and the domain model
        - **Identify** ‘create’ **relationships** from calls to **constructors**
            - If an object of class A invokes a **constructor** of Class B, then A creates B
        - **Identify** ‘use’ **relationships** from classes as **parameters or return types**
        - Class A uses class B if one of the following is true
            - An object of class A passes an object of class B as a **parameter in a function call**
                - On the next slide CheckoutController object calls the save(l:Loan) and save(d:Document) methods of the DBMgr – therefore, CheckoutController uses Loan and Document
            - An object of class A receives an object of class B as a **return value**
                - A receives the B object because it intends to use it - the d:=getDocument(callNo:String): Document call means that CheckoutController uses Document
            - Class B is a **parameter type** of a function of class A
                - DBMgr uses Loan and Document because save(l:Loan) and save(d:Document) are functions of DBMgr
            - Class B is the **return type** of a function of class A
                - DBMgr uses Document because Document is the return type of the getDocument(...): Document function of DBMgr
        - **Identify** ‘association **relationships**’ from calls to **constructors** that pass two **objects as parameters** -- possible **association** class
            - Patron and Document are used to invoke the **constructor** of a loan
        - **Identify** ‘relationships’ from **get** object and **set** object messages
            - A is an aggregation of B if an object of B is a part of an object of A
            - A uses B, as explained in item 2 above (previous chart)
            - A associates with B if A and B are neither of the two above
        - **Identify** ‘call’ **relationships**
            - **call**
        - **Identify** ‘containment’ **relationships** from messages to add, get, or remove objects
            - **containment**
            - These are messages of the form add(b: B), get(…):B, getB(…):B, etc.
        - **Identify additional **relationships** from the **domain model**
            - These are **Inheritance, aggregation, and association relationships**
    - Identify Relationships
    - <img src="img/Identify Relationships.png" alt="Identify Relationships" width="500" height="600">
    - Fill In Relationships
    - <img src="img/Fill In Relationships.png" alt="Fill In Relationships" width="500" height="600">
    - <img src="img/From Sequence Diagram to Implementation.png" alt="From Sequence Diagram to Implementation" width="500" height="600">
    - The Complete DCD
    - <img src="img/The Complete DCD.png" alt="The Complete DCD" width="500" height="600">
    - Another DCD
    - <img src="img/Another DCD.png" alt="Another DCD" width="500" height="600">

16. DCD Review check ist
    - Ensure that **classes, attributes, operations, parameter types, return types, and relationships** in the DCD are derived correctly according to the steps presented in this chapter
    - Ask these questions
        - Does the DCD contain unnecessary **classes, operations, or relationships**?
        - Does the naming of the classes, attributes, operations, and parameters **communicate concisely** the intended functionality and is the intended functionality **easy to understand**?
        - Does the DCD clearly indicate **the design patterns** used?
    - Compute metrics
        - Fan-in, fan-out, class-size, depth in inheritance tree, and coupling between classes and identify potential problems

17. Organize Classes with Package Diagram
    - <img src="img/Organize Classes with Package Diagram.png" alt="Organize Classes with Package Diagram" width="500" height="600">

18. Applying Agile Principles
    - Value working software over comprehensive documentation
    - Good enough is good enough

19. Deriving a Design Class Diagram Cahpter
    - This chapter discussed the steps to building **the Design Class Diagram** (DCD)
        - **Blue print** for the design
        - **Collects and Integrates** design work products from the previous process steps into this blue print of the system
    - Also presented how to use **package diagrams** to organize the classes of the DCD if the DCD is too cumbersome, unwieldy

20. User Interface Design
    - User interface design is concerned with the design of **the look and feel** of the user interfaces
    - The design for **change, separation of concerns, information-hiding, high- cohesion, low-coupling, and keep-it-simple-and-stupid software design** principles should be applied during user interface design
    - **User’s feelings** about the UI greatly influences their **acceptance** of the system and success of our project

21. User Interface (UI) Design
    - The UI is the primary means used by system users to interact with the system
        - Text-based systems (not so much these days)
        - Graphical User Interfaces (GUIs)
    - Characteristics of GUIs
        - Windows (multiple windows, tasks)
        - Easy to learn (placements, gadgets, etc.)
        - Multimedia presentation/communications
    - UI design usually involves
        - Layout design for windows and dialog boxes
        - Design of interaction behavior
        - Design of information presentation schemes
        - Design of online support

22. UI Design is Important
    - UI is the sole **communication channel** between the system and the user
        - User uses this **channel** to conduct business
        - If UI is **easy to use**, this typically leads to **increased productivity and quality**
        - If UI is **not so easy**, **users tend to avoid** using the system or there may be more errors to deal with in the work output
    - Importance of UI is often under-stated
        - Lots of stories
    - GUI Guidelines
        - Minimize numbers of windows & dialog boxes to perform a business task
        - Use selection inputs to reduce human text input errors
        - Partition long lists into hierarchies of shorter lists for user selection
        - Apply abstraction when information to be displayed is same kind
        - Apply classification (e.g., subjects, categories, subcategories) when information belongs to different categories
    - Really, who cares
        - Your system users

23. User Interface Design Process
    - <img src="img/User Interface Design Process.png" alt="User Interface Design Process" width="500" height="600">
    - Identify major system displays
    - Produce a draft design of windows and dialogs
    - Specify interaction behavior
    - Implement a prototype, if desired
    - Evaluate UI design with users
    - Explanation of steps
        - Identify major system displays
            - The **major system displays, user input and user actions** are identified from the expanded use cases produced in the current iteration. These form the basis for the design of **the look and feel** in the next two steps
        - Produce a **draft design** of the **user interface**
            - Develop a draft layout of the user interface (including windows and dialog boxes) corresponding to the major systems displays (previous step) is produced. This step designs the “look” of the user interface
        - Specify the interaction **behavior**
            - A state diagram is produced to specify the navigation relationships between the user interface items. This step develops the “feel” of the user interface
        - Construct a user interface **prototype**
            - This step produces a user interface prototype to show the look and feel as designed in the previous two steps
        - **Evaluate** the design with users
            - In this step, the user interface design, and possibly the prototype, is presented to a group of user representatives to solicit their feedback

24. State Diagram Editor Example
    - The state diagram editor is **a stand-alone application** that allows the user to draw or edit a state diagram
    - For simplicity, **only** flat state diagrams are considered in the version of the editor – they do not contain other states
    - For the next slide, we are considering the following portion of the requirements
        - “The state diagram editor shall allow a user to edit a new or existing diagram. The editor shall allow a user to add, delete, and edit states and transitions as well as undo and redo these operations. The editor shall allow the user to perform other editing operations including saving a diagram as is.”

25. Edit State Diagram Expanded Use Case
    - <img src="img/Edit State Diagram Expanded Use Case.png" alt="Edit State Diagram Expanded Use Case" width="500" height="600">

26. Edit State Diagram System Displays
    - <img src="img/Edit State Diagram System Displays.png" alt="Edit State Diagram System Displays" width="500" height="600">

27. Step 1 -Identify Major System Displays
    - A major system display is one that user inputs or displays system processing results
        - An error message or a confirmation dialog is not a major system display
    - In this step, the major system displays, the displayed information, as well as associated user input and user actions are identified from the Expanded Use Cases produced in the current increment
    - The following rules are applied in this step
        - Examine the steps in the **right** column of each Expanded Use Case -- A major system display is identified if
            - The step displays system processing results (in this case, the displayed information is also identified), or
            - The step requests the user to supply some input (e.g., text, selection, or other type of input)
        - Examine the steps in the **left** column of each Expanded Use Case to identify user input and user actions associated with each system display

28. Step 2- Produce a Draft Layout Design
    - In this step, the following activities are performed
        - Derive windows and dialog boxes for the system displays identified in the previous step
            - Produce a draft design for these windows and dialog boxes
        - User input and user actions are specified in the Expanded Use Cases
            - Widgets contained in each window or dialog are derived
        - The next slide shows the windows, dialogs, and widgets identified for the state diagram editor
        - Windows, Dialogs and Widgets
        - <img src="img/Windows, Dialogs and Widgets.png" alt="Windows, Dialogs and Widgets" width="500" height="600">
        - The next part of this presentation deals with design aspects of user interface and should be used when applying a draft layout design
    - It is important to stay with existing industry layouts to facilitate user learning curve
        - For example, there are consensus layout designs for the user interfaces of IDEs and document editors
        - Know industry guidelines or standards for UI in your industry
    - Layout Design of State Diagram Editor
    - <img src="img/Layout Design of State Diagram Editor.png" alt="Layout Design of State Diagram Editor" width="500" height="600">



        
    
        


    
        
