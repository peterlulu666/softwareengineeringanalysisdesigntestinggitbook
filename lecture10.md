## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1NKyRxJwbMMf59Qk0_mDeFB4IvRK_87kd/preview"></iframe>

## Quiz        

1. The N-Tier Architectural Style 
    - System is structured into a number of leveled layers or tiers 
    - Interacts with only 1 actor at a time
    - Events arrive in predetermined sequence
    - System must respond to each event

2. Event-Driven Style 
    - Consists of a state-based controller and components under its control
    - Exhibits state-dependent behavior implemented by the controller
    - Components send events to controller
    - Controller processes events and sends instructions to components

3. Main Program and Subroutines Style 
    - Consists of a main program and a number of subprograms/subroutines
    - Subroutines may call additional lower level subroutines
    - Often used as architecture for transformational systems
    - Information processing consisting of network of processes
        - Transforms input to a different output

4. Persistence Framework Style
    - Hides databases/file systems by decoupling them from objects that use them
    - Objects are ‘unaware’ of the specific storage devices used
        - Changes to databases or file systems then have no impact on the objects that use them

5. Client-server Style
    - Consists of a server and some number of clients
    - Clients send requests to the server
    - Server processes requests and sends results back to the clients
    - Clients can be added or deleted from the network without impacting work of the server
        - Clients can be lightweight computers if most services are provided by server

6. Peer-to-peer Style
    - Consists of independent component connected through network protocols
        - Used in distributed or decentralized computing systems
        - Highly scalable, robust
    - State and behavior are distributed among peers which can act as either clients or servers
        - Peers: independent components, having their own state and control thread
        - Connectors: Network protocols, often custom
        - Data Elements: Network messages
        - Topology: Network (may have redundant connections between peers); can vary arbitrarily and dynamically
    - Supports decentralized computing with flow of control and resources distributed among peers
    - Highly robust in the face of failure of any given node
    - Scalable in terms of access to resources and computing power. But
caution on the protocol!

7. Blackboard Style
    - Two kinds of components:
        - Central data structure (data repository) — called “blackboard”
        - Independent components (programs) operate on the blackboard
    - System control is entirely driven by the blackboard state
    - Examples
        - Typically used for AI systems
        - Integrated software environments (e.g., Interlisp)
        - Compilerarchitecture

8. Pipe and filter style
    - Components are filters (programs)
        - Transform input data streams into output data streams using connectors (pipes)
        - Also known as pipeline architecture style
    - Filters are independent (no shared state)
        - Filters have no knowledge of up- or down-stream filters
    - Variations
        - Pipelines — linear sequences of filters
        - Bounded pipes — limited amount of data on a pipe
        - Typed pipes — data strongly typed
    - Advantages
        - System behavior is a succession of component behaviors
        - Filter addition, replacement, and reuse
            - Possible to hook any two filters together
        - Concurrent execution
    - Disadvantages
        - Batch organization of processing
        - Interactive applications
    - Examples
        - UNIX shell, signal processing
        - Distributed systems, parallel programming
    - Example: ls invoices | grep -e August | sort 

9. Data flow style
    - Batch Sequential Style
        - Separateprogramsareexecutedinorder
            - Data is passed as an aggregate from one program to the next
        - Connectors: “The human hand” carrying tapes between the programs, a.k.a. “sneaker-net ”
        - Data Elements: Explicit, aggregate elements passed from one component to the next upon completion of the producing program’s execution
    - Typical uses
        - Transaction processing in financial systems
            - “The Granddaddy of Styles”

10. Rule based style
    - Inference engine parses user input and determines whether it is a fact/rule or a query
        - If it is a fact/rule, it adds this entry to the knowledge base
        - Otherwise, it queries the knowledge base for applicable rules and attempts to resolve the query
    - Components: User interface, inference engine, knowledge base
    - Connectors: Components are tightly interconnected with direct procedure calls and/or shared memory
    - Data Elements: Facts and queries
    - Behavior of the application can be very easily modified through addition or deletion of rules from the knowledge base
    - Caution: When a large number of rules are involved, understanding the interactions between multiple rules affected by the same facts can become very difficult

11. Interpreter style
    - Interpreter parses and executes input commands, updating the state maintained by the interpreter
        - Components:Commandinterpreter,program/interpreterstate, user interface
        - Connectors: Typically very closely bound with direct procedure calls and shared state
        - Highly dynamic behavior possible, where the set of commands is dynamically modified
        - System architecture may remain constant while new capabilities are created based upon existing primitives
        - Superbforend-userprogrammability
            - Supportsdynamicallychangingsetofcapabilities 
            - e.g.,LispandScheme

12. Mobile style
    - Mobile-Code Style -- a data element (some representation of a program) is dynamically transformed into a data processing component
        - Components: “Execution dock”, which handles receipt of code and state; code compiler/interpreter
        - Connectors: Network protocols and elements for packaging code and data for transmission
        - Data Elements: Representations of code as data; program state; data
        - Variants: Code-on-demand, remote evaluation, and mobile agent.

13. Publish subscribe style
    - Subscribers register/deregister to receive specific messages or specific content
    - Publishers broadcast messages to subscribers either synchronously or asynchronously
        - Components: Publishers, subscribers, proxies for managing distribution
        - Connectors: Typically a network protocol is required. Content-based subscription requires sophisticated connectors.
        - Data Elements: Subscriptions, notifications, published information
        - Topology: Subscribers connect to publishers either directly or may receive notifications via a network protocol from intermediaries
        - Qualitiesyielded:Highlyefficientone-waydisseminationof information with very low-coupling of components

14. Design your own architecture
    - Reusing an architectural style is usually preferred 
        - Savestime
        - Ensures a level of quality
    - Not all application systems development projects can reuse an existing architectural style
        - Custom architectural design may be required to meet the needs of an application system
    - Design patterns are useful for custom architectural design
       - Known solutions to common design problems
       - Covered in a later module

15. Specify subsystem function and interface
    - Once the architecture is developed for the system, requirements are typically allocated to subsystems
    - Architectural design continues with:
        - Allocate requirements and architectural design objectives to subsystems and components
        - Specify the functionality of each subsystem and component
        - Specify the interfaces of the subsystems and components
        - Specify the interaction behavior of the subsystems and components

16. Review architectural design
    - Architectural design is an iterative process
    - Review the design to ensure that requirements are allocated correctly and architectural design objectives are met
        - With Architectural Design Team
        - With other stakeholders
    - Review to ensure that the architectural design satisfies software design principles
    - Review to ensure that the architectural design satisfies security requirements, constraints, and secure software design principles (i.e., the non-functional requirements and constraints)

17. Architectural design and package diagram
    - Software Architecture defines structure of the software system, subsystems and relationships
        - Components 
        - Connections 
        - Constraints
    - Software architecture provides for:
        - Conceptualization – helps design team think of the software in terms of its
overall structure – provides a working system model for the team
        - Construction – helps team organize the software development work providing a reference model for team for the development builds
        - Managing -- helps team organize and keep organized the software development and software process artifacts (i.e., classes, use cases, data, etc.) during development and construction (see package diagram as example)
    - Evolving – establishes a ‘system model’ from which to evolve and expand the developed system
    - The package diagram gives us a way to organize the software artifacts of
the development process
    - Provides a way to realize the benefits of ‘architecture’

18. Applying software design principles
    - Design principles are widely accepted rules for software design; correctly applying these rules leads to better quality designs
        - Design for Change – design with a “built-in mechanism” to adapt to or facilitate anticipated changes (see section 6.2 in textbook)
        - Separation of Concerns – focus on one aspect of the problem in **isolation** rather than tackling all aspects at the same time
        - Information Hiding – shield implementation detail of a module to reduce its change impact to other parts of the software system
        - High Cohesion – from modular design in SA/SD, achieve a higher degree of relevance of the module’s functions to the module’s core functionality
        - Low Coupling – from SA/SD, reduce the run-time effect and change impact of a subsystem to other subsystems
        - Keep It Simple and Stupid – design simple and “stupid objects”

19. Guideline for architectural design
    - Adapt an architectural style, when possible
        - Saves time and effort, typically
        - Based on “type of system”
    - Apply software design principles
        - Brings quality and features to the product design
    - Apply design patterns
        - Save time and effort
        - Provides known tried solutions to common problems
    - Check architecture against design objectives and design principles
        - Ensures these were not lost or forgotten during design work
    - Iterate the steps
        - And review

20. Actor-System Interaction Modeling
    - Actor-system interaction modeling is modeling and design of how the system interacts with the actors to carry out the use cases
    - Actor-system interaction modeling is accomplished by constructing a two-  column table that describes, for each interaction, the actor input and  actor action, and the system response
    - Previously we discussed how to derive use cases from requirement
        - Mentioned 3 levels of use case abstraction
            - Abstract Use Cases  -- a verb-noun use case name 
            - High Level Use cases  — an Abstract use case plus a description of TUCBW and TUCEW 
            - Expanded Use Case  — a description of how the actor interacts with the system. This is a continued refinement of the high-level use cases using a 2-column table format. (That's what this module is about!) 

21. Deriving Use Cases from Requirements 
    - In the requirements specification, look for "verb-noun" phrases that indicate domain specific activities
        - "do something"
        - something must be done"
        - "perform some task"
    -  Verify the "verb-noun" phrases using use case definition
        - (1) Is it a business process? y/n 
        - (2) Is it initiated by an actor? y/n 
        - (3) Does it end with the actor? y/n 
        - (4) Does it accomplish something useful for the actor? y/n
        - All the answers to above questions must be "y"
    - From the requirements, identify also
        - Actors, who initiate the tasks, or for whom the tasks are performed
        - System or subsystem that contains the use case

22. Use Case Specification: 3 Levels of Abstraction
    - Abstract use case: a verb-noun phrase - appears on the UCD 
    - High level use case: when and where the use case begins and ends 
        - TUCBW (This use case begins with ...) 
        - TUCEW (This use case ends with ...)
    - Expanded use case: step-by-step description of how the actor interacts with the system to carry out the business process
    - Example: 
        - Abstract use case: Initiate a Call 
        - High level use case: 
            - Use Case: Initiate a Call 
                - TUCBW the caller picks up the handset from the phone base. 
                - TUCEW the caller hears the ring tone. 

23. High Level Use Case Examples
    - Use Case: Withdraw Money (from an ATM) 
        - TUCBW the ATM user inserts an ATM card into the card slot. 
        - TUCEW the ATM user receives the correct amount of cash and a withdraw slip. 
    - Use Case: Search for Programs 
        - TUCBW a SAMS user clicks the "Search for Programs" link on any of the SAMS pages. 
        - TUCEW the user sees a list of programs satisfying the search criteria. 

24. Guidelines for High Level Use Case
    - A high level use case should not specify background processing 
        - Do not specify how the system processes the request such as update the database 
        - Background processing is modeled by sequence diagrams to be done later 
    - High level use cases should end with what the actor wants to have accomplished

25. Review: Three Levels of Use Case Specification
    - Abstract use case: using a verb and a noun phrase
    - High level use case: stating exactly when and where the use case begins and ends using .. . 
        - TUCBW ... (This use case begins with ...) 
        - TUCEW (This use case ends with ...) 
    - Expanded use case: describing step-by-step how the actor and the system interact to accomplish the business task using a two-column table
        - Always starts with the System displaying some information (status/message/web page) as **Step 0** 
        - Step 1 picks up the "TUCBW . . . ." 
        - Always ends on an actor step with confirmation that the actor sees/acknowledges the use case has provided the desired **response**. This is the "TUCEW..." 
        - It always has a **pre-condition** and **post-condition** (discussed next) 









    


