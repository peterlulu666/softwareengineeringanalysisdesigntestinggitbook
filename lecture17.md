## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1vqIL7ZnQCAyVJmrIKcpNpo7cu2dA9VJE/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1U_tss8PaktPk8Z7sY7MLUSv8tUHxJI8s/preview"></iframe>

## Quiz      

1. Why Activity Modeling
    - Systems analysis and design needs to
        - Model and describe current information processing activities in the organization or the existing system (i.e., modeling of the existing system) to help the software development team understand the existing business processes
        - Describe information processing with the proposed solution (i.e., produce a system design of new information processing)
    - UML Activity Diagrams help software design engineers understand, design, and communicate complex information processing

2. The Activity Diagram
    - An activity diagram models the information processing activity in the real world (analysis model) or the system (design model)
    - A UML activity diagram is a combination of
        - Flowchart diagram
            - For decision-making or branching
        - Data flow diagram
            - For information processing and data flows
        - Petri net diagram
            - For various control flows
            - For synchronization, concurrency, forking, and joining

3. A Flowchart
    - The decision making
    - <img src="img/A Flowchart.png" alt="A Flowchart" width="500" height="600">

4. A Data Flow Diagram
    - The data flow
    - The information processing activity
    - <img src="img/A Data Flow Diagram.png" alt="A Data Flow Diagram" width="500" height="600">

5. Petri Net
    - **places**, representing an abstract condition
    - **transitions**, representing an event or happing
    - **relationship** between a place and a transition, it can only come from a place to a transition or from a transition to a place
    - **tokens**, which can be placed in places to indicate that the condition is **True**
    - <img src="img/Petri Net.png" alt="Petri Net" width="500" height="600">

6. A Petri Net Example
    - <img src="img/A Petri Net Example.png" alt="A Petri Net Example" width="500" height="600">

7. Petri Net Execution
    - <img src="img/Petri Net Execution.png" alt="Petri Net Execution" width="500" height="600">

8. Petri Net Markine
    - <img src="img/Petri Net Markine.png" alt="Petri Net Markine" width="500" height="600">
    - <img src="img/Petri Net Markine job.png" alt="Petri Net Markine job" width="500" height="600">

9. Analysis of Petri Net
    - <iframe height="842" width="100%" src="img/Analysis of Petri Net.pdf"></iframe>

10. Petri Net Expressiveness
    - parallelism
    - sequencing
    - synchronization
    - exclusion
    - <img src="img/Petri Net Expressiveness.png" alt="Petri Net Expressiveness" width="500" height="600">

11. Activity Diagram Notions and Notations
    - <img src="img/Activity Diagram Notions and Notations.png" alt="Activity Diagram Notions and Notations" width="500" height="600">

12. Box Office Order Processing
    - <img src="img/Box Office Order Processing.png" alt="Box Office Order Processing" width="500" height="600">

13. Activity Diagram: Swim Lane
    - <img src="img/Activity Diagram Swim Lane.png" alt="Activity Diagram Swim Lane" width="500" height="600">

14. Activity Decomposition and Invocation
    - A complex activity can be decomposed and represented by another activity diagram
    - The rake-style symbol is used to signify that the activity has a more detailed activity diagram
    - <img src="img/Activity Decomposition and Invocation.png" alt="Activity Decomposition and Invocation" width="500" height="600">

15. Expansion Region
    - An expansion region is a subset of activities or actions that should be **repeated** for each element of a collection
    - The **repeated** region may produce one or more collections
    - <img src="img/Expansion Region.png" alt="Expansion Region" width="500" height="600">

16. Using Activity Diagram
    - Modeling, analysis, and design of complex information **processing activities** involving one or all of the following
        - Control flows 
        - Object flows or data flows 
        - Access to databases or data depositories 
        - Conditional branching 
        - Concurrent threads 
        - Synchronization
    - Work flow among **multiple** organizational units and/or subsystems
    - Activity diagram can be used **alone** or used with the **other** diagrams
    - Activity diagram can be used to **model** the information processing activity of a system, subsystem, or component, or a  method of a class

17. Steps for Activity Modeling
    - <img src="img/Steps for Activity Modeling.png" alt="Steps for Activity Modeling" width="500" height="600">

18. Relation to other Diagrams
    - An activity may be a use case or suggest a **use case** 
        - Therefore, activity modeling is useful for identifying use cases 
    - Activity diagrams are useful for showing workflows and control flows among **use cases** 
    - Activity modeling is useful for processing complex requests in a **sequence diagram** 
    - An activity may exhibit state-dependent behavior which can be refined by **state modeling** 
    - A state may represent a **complex process** which can be modeled by an activity diagram 
    - Each object sent from one activity to another should appear in the **Design Class Diagram** (DCD) or the domain model 
    - Swim lanes may suggest object classes in the **domain model** or the DCD 
        - The activities of the swim lane identify operations for the class 
    - A complex activity may decompose into **lower-level activities** 
        - Some of these may be operations of the class

19. Activity Modeling
    - Activity modeling combines 3 prominent modeling tools: 
        - Flowchart 
        - Data flow diagrams 
        - Petri-nets 
    - Approach to building Activity Diagrams 
        - Identify information processing information 
        - Construct a preliminary Activity diagram 
        - Introduce branching, forking, joining 
        - Decompose/Refine complex activities 
        - Review and get feedback on Activity Diagrams 
    - Activity diagrams are useful for modeling transformation systems and for modeling complex information flows found in business process engineering

20. The Agile Project
    - The members in an Agile project communicate with each other **early** and **frequently** 
    - The Agile Manifesto contains four statements of values: 
        - Individuals and interactions over processes and tools 
        - Working software over comprehensive documentation 
        - Customer collaboration over contract negotiation 
        - Responding to change over following a plan 
    - The 4 Core Agile Values are further documented in 12 Agile Principles
    - Individuals and Interactions 
        - Agile development is very “**people-centered**” 
        - It is through continuous **communication** and **interaction** that teams work most effectively 
    - Working Software 
        - From a customer perspective, **working software** is much more useful and valuable than **overly detailed documentation** 
        - Working software (reduced functionality) is available much **earlier** in the development lifecycle provides significant time-to-market advantage
    - Customer Collaboration 
        - Customers often find great difficulty in specifying the system that they **require** 
        - Collaborating directly with the customer **improves** the likelihood of understanding exactly what the customer **requires** 
    - Responding to Change 
        - Change is inevitable in software projects. These factors must be accommodated by the development process 
        - Flexibility in work practices and planning to embrace **change** is more important than simply adhering rigidly to a plan

21. The Agile Manifesto
    - Principles - the core Agile Manifesto values are captured in twelve principles:
        - Our highest priority is to satisfy the customer through **early** and **continuous** delivery of valuable software 
        - Welcome **changing** requirements, even late in development. Agile processes harness **change** for the customer's competitive advantage 
        - Deliver working software **frequently**, at intervals of between a few weeks to a few months, with a preference to the shorter timescale 
        - Business people and developers must work **together** daily throughout the project 
        - Build projects around **motivated** individuals. Give them the **environment** and **support** they need and **trust** them to get the job done 
        - The most efficient and effective method of **conveying information** to and within a development team is face-to-face conversation 
        - Working **software** is the primary measure of progress
        - Agile processes promote **sustainable** development. The sponsors, developers, and users should be able to maintain a constant pace indefinitely 
        - Continuous attention to **technical** excellence and good **design** enhances agility 
        - Simplicity—the art of maximizing the amount of work **not done**—is essential 
        - The best architectures, requirements, and designs emerge from **self-organizing** teams 
        - At regular intervals, the team reflects on how to become more effective, then tunes and **adjusts** its behavior accordingly

22. Agile Is A Whole Team Approach
    - Whole-Team Approach
        - The whole-team approach means involving **everyone with the knowledge and skills** necessary to ensure project success 
        - The team includes representatives from the **customer** and other business **stakeholders** who determine product features 
        - The team should be relatively **small**; successful teams have been observed with as few as three people and as many as nine 
        - The whole-team approach is supported through the daily stand-up meetings involving all members of the team, where work progress is **communicated** and any **impediments** to progress are highlighted 
        - The whole-team approach **promotes** more effective and efficient team dynamics

23. Extreme Programming
    - There are several Agile approaches, each of which implements the **values and principles** of the Agile Manifesto in different ways. In this presentation, three representatives of Agile approaches are considered
        - Extreme Programming (XP)
        - Scrum
        - Kanban
    - Extreme Programming
        - XP embraces **five values** to guide development: communication, simplicity, feedback, courage, and respect
        - XP describes a **set of principles** as additional guidelines: humanity, economics, mutual benefit, self- similarity, improvement, diversity, reflection, flow, opportunity, redundancy, failure, quality, baby steps, and accepted responsibility
        - XP describes **thirteen primary practices**: sit together, whole team, informative workspace, energized work, pair programming, stories, weekly cycle, quarterly cycle, slack, ten-minute build, continuous integration, test first programming, and incremental design
        - Many of the Agile software development approaches in use today are influenced by XP and its **values and principles**. For example, Agile teams following Scrum often incorporate XP practices
    - <img src="img/Extreme Programming.png" alt="Extreme Programming" width="500" height="600">

24. Scrum
    - Scrum is an Agile management framework which contains the following constituent instruments and practices: 
        - **Sprint**: Scrum divides a project into **iterations** (called sprints) of fixed length (usually two to four weeks) 
        - **Product Increment**: Each sprint results in a potentially **releasable** and **shippable** product (called an increment) 
        - **Product Backlog**: The product owner manages a **prioritized** list of planned product items. The product backlog evolves from sprint to sprint (called backlog refinement) 
        - **Sprint Backlog**: At the start of each sprint, the Scrum team selects a set of highest priority items (called the sprint backlog) from the product backlog 
        - **Definition of Done**: To make sure that there is a potentially **releasable** product at each sprint’s end, the Scrum team discusses and defines appropriate criteria for sprint completion 
        - **Time-boxing**: Only those tasks, requirements, or features that the team **expects to finish** within the sprint are part of the sprint backlog. If the development team **cannot finish** a task within a sprint, the associated product features are removed from the sprint and the task is moved back into the product backlog 
        - **Transparency**: The development team reports and updates sprint **status** on a daily basis at a meeting called the daily scrum
    - Scrum defines three team roles
        - **Scrum Master**: **ensures** that Scrum **practices** and **rules** are implemented and followed, and **resolves** any violations, resource issues, or other impediments that could **prevent** the team from following the **practices** and **rules**. This person is not the team lead, but a **coach**
        - **Product Owner**: **represents** the customer, and generates, maintains, and prioritizes the product backlog. This person is not the team lead 
        - **Development Team**: **develop** and **test** the product. The team is self- organized: There is no team lead, so the team makes the decisions

25. Scrum Process
    - <img src="img/Scrum Process.png" alt="Scrum Process" width="500" height="600">

26. User Stories
    - Collaborative User Story Creation 
        - User stories are written to “capture requirements” from the perspectives of developers, testers, and business **representatives**
        - This shared vision is accomplished through frequent **informal reviews** while the requirements are being written 
        - The user stories must address both **functional** and **non-functional** characteristics. Each story includes acceptance **criteria** for these characteristics and is complete when the **criteria** have been satisfied
    - According to the 3C concept, a user story is the conjunction of three elements: 
        - Card 
        - Conversation 
        - Confirmation (acceptance criteria)

27. Retrospectives
    - In Agile development, a retrospective is a meeting held at the end of each iteration to discuss what was **successful**, what could be **improved**, and how to **incorporate** the improvements and retain the successes in future iterations 
    - Retrospectives can result in **improvement** decisions focused on effectiveness, productivity, and team satisfaction 
    - They may also address the testability of the applications, user stories, features, or system interfaces. Root cause analysis of defects can drive testing and development **improvements** 
    - In general, teams should implement only a few **improvements** per iteration. This allows for continuous **improvement** at a sustained pace

28. Continuous Integration
    - Continuous Integration 
        - Delivery of a product increment requires reliable, working, **integrated** software at the end of every sprint 
        - Continuous **integration** addresses this challenge by **merging** all changes made to the software and integrating all **changed** components regularly, at least once a day 
        - Configuration management, compilation, software build, deployment, and testing are **wrapped** into a single, automated, repeatable process 
        - Since developers **integrate** their work constantly, build constantly, and test constantly, defects in code are detected more quickly 
        - Code is **refactored** continuously as well

29. Release and Iteration Planning
    - For Agile lifecycles, two kinds of planning occur: 
        - Release planning 
        - Iteration planning
    - Release planning 
        - Looks ahead to the **release** of a product, often a few months ahead of the start of a project 
        - **Defines** and **re-defines** the product backlog and may involve **refining** larger user stories into a collection of smaller stories 
        - Provides the basis for a **test** approach and **test** plan spanning all iterations. Release plans are typically high-level 
        - Business representatives, customers establish and prioritize the user **stories** for the release 
        - Based on these user stories, project and quality **risks** are identified and a high-level effort estimation is performed
    - Iteration planning 
        - Looks ahead to the end of a single **iteration** and is concerned with the iteration backlog 
        - The team selects user **stories** from the prioritized product backlog, elaborates the user **stories**, performs a **risk** analysis for the user stories, and estimates the work needed for each user story 
        - The number of **stories** selected is based on established team **velocity** and the estimated **size** of the selected user stories 
        - After the contents of the iteration are finalized, the user **stories** are broken into **tasks**, which will be carried out by the appropriate team members

30. Daily Stand Up Meeting
    - The daily stand-up meeting includes all members of the Agile. At this meeting, they communicate their current status
    - The agenda for each member is **Agile Alliance Guide**: 
        - What have you completed since the last meeting? 
        - What do you plan to complete by the next meeting? 
        - What is getting in your way?
    - Any issues that may block test progress are communicated during the daily stand-up meetings 
        - So the whole team is aware of the issues and can **resolve** them accordingly

31. Kanban
    - Simply, Kanban is a scheduling system for just-in-time development or manufacturing (Japan)
        - A principle from lean manufacturing, back in the 1990s -- Kanban cards have roots back to the 1940s 
        - Schedule items are ‘pulled’ through the manufacturing system 
        - Uses Kanban cards (sticky notes)
    - Originally, Kanban was to
        - Streamline inventory management 
        - Improve process **efficiency** 
        - Reduce operating **costs**
    - Today, Kanban is implemented in various software development tools modeling the Kanban cards, whiteboards, and sticky notes
        - A Kanban card represents a single **task** (i.e., task title, task description, who assigned, due dates, effort) and the card gets placed onto a white board 
        - Tasks move from **left to right** on the project whiteboard like sticky notes –indicating progress 
            - Anyone can just look where the card is on the board and quickly assess **status** of that task
    - Kanban method today is meant to serve as a central hub of information for the development team
        - Reduce **time** in meetings
        - Facilitate hand-offs between team members
        - Improve overall team **efficiency**

32. Implementation Considerations
    - Everyone in the team should follow the same coding/programming **standards**
    - Test-driven development, pair programming, and code review **improve** the quality of the code
    - Classes should be implemented according to their dependencies to **reduce** the need for test stubs

33. Coding Standards
    - Define the **required** and **optional** items
    - Define the **format** and **language** used to specify required and optional items
    - Define the coding **requirements** and **conventions**
    - Define the **rules** and **responsibilities** to create and implement the coding standards, as well as review and improve the practice

34. Why Coding Standards
    - If we all use the same programming standard on the project, then
        - **Integration** and **test** should be easier
        - **Reuse** of code and other artifacts should be easier
        - **Maintenance** after delivery should be easier
            - “**Maintainable** code”, consistency in programming 
            - **Consistent** style and conventions 
            - Easier to **read** and **understand** the code faster
        - Programmers will know what is **required** to be included (instead of guessing)
    - There are typically two types of programming instructions
        - Style guides
            - Code layout, indentation (formatting conventions), comment density and placements, naming conventions, header templates, etc.
        - Coding standards
            - Establish specifications for applying coding constructs to the specific project
                - May incorporate style guide
                - Defines how to use language, syntax, identifies limitations and what not to do, examples

35. Components of Coding Standards
    - File Header (may be a required templated format) 
        - Typically includes file location, version number, author, project, update history, and other items that may be required by the organization 
            - i.e., requirements, related documents, function description, reviewers, etc.
    - Description of classes 
        - List and functional description for each class including: 
            - Purpose 
            - Description of methods 
            - Description of fields 
            - In-code comments
    - Conventions 
        - Naming 
        - Formatting 
        - Commenting
    - Rules with rationale (the reasons the rule is applied), examples, exceptions, and notes
    - How compliance will be handled

36. Programming Standard Considerations
    - Test is the most effective and efficient when certain test issues are considered during the design of the software
        - Limit (or restrict) the use of **exception** handling 
            - Important because it can be over-used when proper guidance is not provided. Exception handling should not be used to address normal case conditions (e.g., a decision statement). Its use should be very sparse or not all at 
            - Generation of **exception** conditions during test can range from difficult to impossible
        - Limit and prune the use of deep **inheritance** structures 
            - Deep **inheritance** structures can suffer from the yo-yo problem 
            - Deep **inheritance** structures can make test very complicated – sometimes requiring the entire class hierarchy to be utilized just to perform a simple unit test
        - Be careful when utilizing **polymorphism**
            - Static vs. dynamic binding (generics) 
            - Dynamic binding can be very difficult to test
        - Encapsulation/information hiding is a great design technique 
            - But it may be worth considering adding extra methods exclusively for testing just to provide additional visibility into the **internals** of a class
        - Consideration should be given for creating limits on cyclomatic complexity (to reduce test size) 
            - Edges – Nodes + 1 
        - Consideration should be given on essential complexity (restricting unstructured code) 
            - Remove structured programming primitives from the control flow graph and then compute cyclomatic complexity 
        - Consideration should be given on restricting use of short-circuiting of logical expressions (use bit-wise instead)  
            - Short circuiting – when 2 conditions in an expression are evaluated (such as a Boolean or an OR, for example), when first condition evaluates true then the overall expression is true 
    - These items should be addressed in a coding standards and automatically checked by static code analyzers as an entry criteria for a technical review (e.g., peer review, Fagan Inspection, etc.) 
        - For programming standards compliance

37. Coding Conventions
    - Naming conventions specify rules for naming packages, modules, paths, files, classes, attributes, functions, constants, and the like 
    - Formatting conventions specify formatting rules for line breaking, indentation, alignment, and spacing 
    - In-code comment conventions define the requirements and guidelines for writing in-code documentation




















































