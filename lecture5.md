## Quiz        

1. The OO context diagram
    - The OO view of the system.
    - The context  of subsystem.
    - Model environment and subsystem using object that interact.        

2. The other software engineering activities in the system engineering group
    - Develop subsystem
        - Developed by different team.
        - Collaborate to solve interdisciplinary problem. 
    - The system integration, testing, and deployment

3. The system configuration management
    - Ensure update system version
    - SCM
        - Configuration identification
        - Configuration change control
        - Configuration auditing
        - Configuration status reporting

4. The software requirement elicitation
    - Requirement is the capability that deliver.    
    - The hard part is what to build.      
    - It is the requirement for the system, but not the user asked for.      
    - The constraint is the restriction on the solution space. The requirement is on the problem space.      
    - The requirement elicitation is process to identify capability for the software system.      

5. The requirement elicitation activities
    - Identify problem.
    - Use model to help understand problem.      
    - Formulate system requirement.
    - Use feasibility study to lower the risk.      
    - Work together and consistent.      

6. The importance of the requirement elicitation
    - contract legal consequence.      
    - Software problem relate to requirement.      
    - Increase cost and lead to failure if there is no requirement.      
    - What to build.      

7. The problem of the requirement elicitation
    - The lack of expertise.
        - People think they know more than what they know.       
    - The incomplete idea.
    - The non functional requirement and constraint is forgotten.
    - The difficulty of using complex tool.      

8. The root cause of the problem
    - The insufficient requirement and ad hoc management.
    - They do not precisely communicate.
    - Brittle architecture.
    - Complexity.
    - Inconsistency.
    - Insufficient understanding.
    - Status.
    - Risk.
    - Propagation.      

9. Why requirement?
    - NIST.
    - A lot percentage.

10. The chanllenge of the requirement elicitation
    - Wicked problem.
    - Communication.
    - The importance is under estimated.
    - The non functional requirement is under estimated.
    - The requirement change.      

11. The importance of the requirement
    - The biggest cause of problem.
    - Requirement help you undersatand what is the right design, hardware, and software.
    - Requirement drive everything.
    - They begin to solve the problem before they agree what is the problem.

12. What is requirement?
    - Use shall. It is required. 
    - Use will to express desired feature not required.

13. The type of requirement
    - The functional requirement
        - The process capability system possess.
        - The car rental system.
        - The study abroad system.
    - The non functional requirement
        - what, how, how fast, how good.
        - The performance requirement.
        - The quality requirement.
            - The user friendliness.
            - The user interface.      
            -The UI standard.      
        - The safety requirement.
        - The security requirement. 
        - The interface requirement.
        - The reliability requirement.
        - The dependability requirement.
        - The workload.
        - The responsetime.
        - The development method.      

14. The requirement elicitation step
    - Collect info.
        - The presentation.
        - The survey.
        - The interviewing. 
        - Writing story. 
    - Construct analysis model.
        - Help understand application, requirement, and constraint.
    - Derive requirement and constraint.
        - The current situation, business goal, problem, standard, policy will help us to create the requirement.      
    - Conduct feasibility study.
        - If it is under constraint.
        - In the requirement engineering, it is about feasibility of constraint, adequacy of technology, time and cost constraint.  
    - Review.
        - The peer review, least cost.
        - The walkthrough, more cost.
        - The inspection, highest cost.
            - The incompleteness, duplicate, and inconsistency.
        - The expert review.
        - The user review.      

    
    









        

























