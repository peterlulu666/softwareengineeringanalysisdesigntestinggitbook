## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1FztTn16fuguYtSSAvIa4F8lKU8XkvX05/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1vqIL7ZnQCAyVJmrIKcpNpo7cu2dA9VJE/preview"></iframe>

## Quiz          

1. The Object State Modeling
    - Identification, modeling, analysis, design, and specification of state- dependent reactions of objects to external stimuli
        - What are the external stimuli of interest?
        - How does one characterize the states to determine whether an object is in a certain state?
        - How does one identify and represent the states of a complex object?
        - How does one identify and specify the state-dependent reactions of an object to external stimuli?
        - How does one check for desired properties of a state behavioral model?

2. State Behavior Modeling
    - State dependent behavior is common in software systems
    - The start arrow
    - The state diagram
    - <img src="img/State Behavior Modeling.png" alt="State Behavior Modeling" width="500" height="600">
3. Basic Definitions
    - An **event** is some happening of interest or a request to a subsystem, object, or component
    - A **state** is a named abstraction of a subsystem/ object condition or situation that is entered or exited due to the occurrence of an event

4. Object State Modeling Steps
    - <img src="img/Object State Modeling Steps.png" alt="Object State Modeling Steps" width="500" height="600">

5. Constructing a State Transition Table
    - The textbook states that constructing the state transition table is **optional**
        - But, this is the most complete way of assessing state information
        - It is the only method where the states are **validated** across the complete possibilities of the event space
    - To use the state transition table, each **row** in the table represents a **state** with each **column** representing an **event**
- Each state should have each event mapped across all possible combinations for that state
- This ensures that each state has a defined response to each event which ensures that the state model is complete
- State transition tables make it is easy to **detect** the following
    - Dead states (no outgoing transitions)
    - Unreachable states (not reachable - can't get there - logic error in table)
    - Neglected events (a blank column - event not accepted by any state)
    - Impossible transitions (guard condition can never be true)
    - Nondeterministic transitions (faulty logic on guards or destinations)
    - Redundant transitions (transitions that can be reduced)
    - Inconsistent transitions (transitions where guard conditions are identical)

6. State Diagram Notations (UML)
    - <img src="img/State Diagram Notations (UML).png" alt="State Diagram Notations (UML)" width="500" height="600">

7. UML Statechart
    - A UML state chart is actually a Harel statechart
        - Expanded notation of the simpler Mealy or Moore state diagrams
    - Like state transition diagrams, UML state diagrams are **directed graphs**
        - Nodes denote states and connectors transitions
    - **Event** – is a type, not a specific occurrence
        - Token is an event for the turnstile, but each specific token is an instance of the Token event
        - For the Push event, if the Push event occurred at a specific time it would be an instance of the Push event
        - An event instance outlives the occurrence that caused it. A specific event instance can goes through three stages
            - **Received** (on the event queue)
            - **Dispatched** to the state machine, at which point it becomes the current event
            - Consumed when the state machine finishes processing the event instance
    - A **state** is much like the previous but captures the specific aspects of system behavior in a more focused manner (a single state variable)
    - **Guards** - Boolean expressions evaluated dynamically based on the value of event parameters. They affect the behavior only when they evaluate to **TRUE** and disable them when evaluated to **FALSE**
    - **Actions** – the dispatching of an event causes the state machine to perform actions
    - **Transitions** -- switching from one state to another. The event that caused the transition is called the trigger
    - **Run to completion** - Incoming events cannot interrupt the processing of the current event and must be stored until the state machine can process them. This approach avoids any concurrency issues within a single state machine
    - My preference is to keep state machines in UML simple and represent them as either Mealy or Moore machines

8. Moore and Mealy Machines
    - There are two types of graphical depictions of state machines, they differ in the way that outputs are produced
    - **Moore Machine**
        - Outputs are independent of the inputs
            - Outputs are effectively produced from within the state of the state machine
    - **Mealy Machine**
        - Outputs can be determined by the present state alone, or by the present state and the present inputs
            - Outputs are produced as the machine makes a transition from one state to another

9. Mealy Machine Diagrams
    - The **Mealy State Machine** generates outputs based on
        - The Present State, and
        - The Inputs to the Machine
    - So, the Mealy state machine is capable of generating many different patterns of output signals for the same state, depending on the inputs present on the event
    - Outputs are shown on transitions since they are determined in the same way as is the next state
    - Output condition that results from being in a particular present state
    - Input condition that must exist in order to execute these transitions from State 1
    - <img src="img/Mealy Machine Diagrams.png" alt="Mealy Machine Diagrams" width="500" height="600">

10. Simple System
    - Turnstile system
    - A turnstile is used to control access to subways, is a gate with rotating arms (typically 3) near waist height, one across the entryway
    - Initially the arms are locked, preventing entry
    - Depositing a token in a slot on the turnstile unlocks the arms, allowing a single customer to enter.  After the customer enters by rotating through the arms, the arms are locked again until another token is inserted
    - <img src="img/Simple System.png" alt="Simple System" width="500" height="600">

11. Converting to State Diagram
    - <img src="img/Converting to State Diagram.png" alt="Converting to State Diagram" width="500" height="600">

12. Converting Texts to Function Calls
    - <img src="img/Converting Texts to Function Calls.png" alt="Converting Texts to Function Calls" width="500" height="600">

13. Implementing State Behavior
    - Conventional approaches
        - Nested switch approach
        - Using a state transition matrix
        - Using method implementation

14. Conventional Implementation: Nested Switches
    - <img src="img/Conventional Implementation Nested Switches.png" alt="Conventional Implementation Nested Switches" width="500" height="600">

15. Conventional State Transition Matrix
    - <img src="img/Conventional State Transition Matrix.png" alt="Conventional State Transition Matrix" width="500" height="600">

16. Using Method Implementation
    - State behavior of a class is implemented by member functions that denote an event in the state diagram 
    - The current state of the object is stored in an attribute of the class 
    - A member function evaluates the state variable and the guard condition, executes the appropriate response actions, and then updates the state variable
    - All these methods have drawbacks 
        - Rising cyclomatic complexity (independent paths through program) 
        - Higher complexity → harder to quickly understand 
        - Changes get harder with larger SM 
    - We have a state pattern to solve this common design problem!

17. State Pattern
    - <img src="img/State Pattern.png" alt="State Pattern" width="500" height="600">

18. Benefits of State Pattern
    - Significantly reduces the cyclomatic complexity of the state handling code 
    - Easy to add states 
        - Simply add state subclasses and implement the relevant operations 
    - Easy to add transitions 
        - Simply add operations to the State class and implement the operations in relevant State subclasses 
    - Easy to understand, implement, test, and maintain

19. Thermostat State Diagram
    - <img src="img/Thermostat State Diagram.png" alt="Thermostat State Diagram" width="500" height="600">

20. The Season Switch State Pattern
    - <img src="img/The Season Switch State Pattern.png" alt="The Season Switch State Pattern" width="500" height="600">

21. Object Interaction in Season Switch State Pattern
    - <img src="img/Object Interaction in Season Switch State Pattern.png" alt="Object Interaction in Season Switch State Pattern" width="500" height="600">

22. Awareness of the State Design Pattern
    - Be aware this state pattern exists and that it is a solution to a common software design problem 
    - Most state models have some basic, repeated problems 
        - **Avoid states when possible** - most people overuse states. They use them in place of decision logic and this starts down a very deep hole. Once one starts using states, one may have to invent new states to deal with nuances that are not always apparent. If a state does not represent an obvious behavioral state of the system/software, it is not a state.
    - **When using states, make them as simple as possible**. Each state should be the logical complement of the other - and there should be very few of states for the whole system. States by definition are global for the whole system. Whenever one adds a state, since it is global, one can start to experience states clashing with existing states.
    - **When using states, put these in one place only - a "controller" object**. Make everything else as dumb as possible. This limits the impact of state explosion to other portions of the system/software.

23. Alternative Approach State Diagram
    - <img src="img/Alternative Approach State Diagram.png" alt="Alternative Approach State Diagram" width="500" height="600">
    - Using this approach, the AC Relay AR, Furnace Relay FR, and Blower Relay BR are now enumerations only - they have no state information 
        - BR = AR Closed | FR Closed | Fan=On 
    - The state information is contained in the single diagram and will be implemented using the Controller pattern 
    - Each of the devices AR, FR, and BR are objects on the sequence diagram 
        - Since they are simple devices, there is no need for a DeviceMgr (Expert Pattern) 
        - If these devices were more complicated, one could use a device manager to hide the interface, but only if the real world has such an intermediate 
    - Three "GUI" objects - the Set Temp, the Season switch, and the Fan Switch 
        - These are simple devices also and just have ‘getters’ and ‘setters’

24. Climate Controller Analysis Sequence Diagram
    - <img src="img/Climate Controller Analysis Sequence Diagram.png" alt="Climate Controller Analysis Sequence Diagram" width="500" height="600">

25. Guidelines for State Design Pattern
    - **Know the state pattern** - others use it so you need to be able to recognize it and converse about it
    - It seems a little counterintuitive to the whole OO approach - the main benefit gained is to try and centralize state information
    - Guidelines
        - For up to 3 states, use the nested if/switch approach or the transition table . . . these are going to be the simplest approaches and they encapsulate the logic in the controller
        - For 4 - 10 states, it is possible the best solution is the transition table approach -- this lets the implementation get captured in an easy-to-change table and it still encapsulates the logic within the controller. Else use the pattern
        - For >10, you're probably doing something wrong . . . .
    - Familiarity with the State Pattern is mainly for performing maintenance (for existing or legacy systems)
    - State machines are widely used for modeling, design, and testing of real-time embedded systems
        - Considerations – Time and temporal constraints, interrupt handling, concurrency, et al.

26. Object State Modeling
    - Object State Modeling (OSM) is concerned with the identification, modeling, analysis, design, and specification of state-dependent behavior of objects
    - 5 steps of OSM
        - Collect and Classify state behavior
        - Construct domain model
            - Specifies structure and relationships
        - Construct State Transition Tables
            - Systematic approach for specifying state behaviors – helps ID dead or unreachable states, neglected events, impossible or inconsistent transitions
        - Construct/visualize state behavior
        - Review models
    - State pattern provides a reusable proven design for state diagrams with composite states

27. Activity Modeling and Agile
    - Activity modeling deals with the modeling of the information processing activities of an application or a system that exhibits sequencing, branching, and concurrency as well as synchronous and asynchronous behavior
    - Activity modeling is useful for modeling and design of transformational systems
        - These system exhibit sequencing, conditional branching, concurrency, and synchronization

28. What Is Activity Modeling
    - Activity modeling focuses on modeling and design for
        - Complex information processing activities and operations
        - Information flows and object flows among the activities
        - Branching according to decisions
        - Synchronization, concurrency, forking, and joining control flows
        - Business process workflows among the various departments or subsystems
    - Works for transformational systems
        - Batch processing modeling
    - Works for a network of activities
        - Transforms input to an output




























